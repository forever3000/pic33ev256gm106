#include "mcc_generated_files/system.h"
#include "mcc_generated_files/tmr1.h"
#include "mcc_generated_files/pin_manager.h"
#include "can.h"

#define CAR_NO 3

extern int can_tx_flag;
extern int i_10us_flag;
extern int i_10ms_flag;
extern int i_100ms_flag;
extern int i_pwm1_flag;
int flag1,flag2,flag3,flag4;
char out1,out2,out3;

int main(void)
{
    SYSTEM_Initialize();
    InitCAN();
    LED1R_SetDigitalOutput();
    LED1G_SetDigitalOutput();
    LED1B_SetDigitalOutput();
    LED2R_SetDigitalOutput();
    LED2G_SetDigitalOutput();
    LED2B_SetDigitalOutput();
    LED3R_SetDigitalOutput();
    LED3G_SetDigitalOutput();
    LED3B_SetDigitalOutput();
    WINKER_SetDigitalOutput();
    ABS_CLK_SetDigitalOutput();
    
    int GET_DATA[20];
    extern int ABS_GET_DATA[20];
    int i_boot_flag=1;
    int i_led_set=0;
    int i_START_COUNT=0;
    int i_500ms_flag=0;

    while (1)
    {
        if(i_boot_flag)
        {
            if(i_500ms_flag>4)
            {
                i_500ms_flag=0;
                switch(i_led_set)
                {
#if (CAR_NO==1)
                    case 1:
                        LED1R_SetLow();
                        LED1G_SetHigh();
                        LED1B_SetHigh();
                        break;
                    case 2:
                        LED1R_SetHigh();
                        LED1G_SetLow();
                        LED1B_SetHigh();
                        break;
                    case 3:
                        LED1R_SetHigh ();
                        LED1G_SetHigh();
                        LED1B_SetLow();
                        i_led_set=0;
                        break;
                    default :
                        i_led_set=0;
                        break;
#elif (CAR_NO==2)                
                    case 1:
                        LED2R_SetLow();
                        LED2G_SetHigh();
                        LED2B_SetHigh();
                        break;
                    case 2:
                        LED2R_SetHigh();
                        LED2G_SetLow();
                        LED2B_SetHigh();
                        break;
                    case 3:
                        LED2R_SetHigh ();
                        LED2G_SetHigh();
                        LED2B_SetLow();
                        i_led_set=0;
                        break;
                    default :
                        i_led_set=0;
                        break;
#elif (CAR_NO==3)
                    case 1:
                        LED3R_SetLow();
                        LED3G_SetHigh();
                        LED3B_SetHigh();
                        break;
                    case 2:
                        LED3R_SetHigh();
                        LED3G_SetLow();
                        LED3B_SetHigh();
                        break;
                    case 3:
                        LED3R_SetHigh ();
                        LED3G_SetHigh();
                        LED3B_SetLow();
                        i_led_set=0;
                        break;
                    default :
                        i_led_set=0;
                        break;
#endif
                }
                i_led_set++;
            }
        }
        if(can_rx_flag)
        {
            can_rx_flag=0;
            if(canRxMessage.buffer_status == CAN_BUF_FULL)
            {
                rxECAN(&canRxMessage);
                canRxMessage.buffer_status = CAN_BUF_EMPTY;
                CAN_RData();
                
                if(CAN_RX_ID == 0x123)
                {
                    if(i_boot_flag==1)
                    {
#if (CAR_NO==1)
                        LED1R_SetHigh();
                        LED1G_SetHigh();
                        LED1B_SetHigh();
#elif (CAR_NO==2)
                        LED2R_SetHigh();
                        LED2G_SetHigh();
                        LED2B_SetHigh();
#elif (CAR_NO==3)
                        LED3R_SetHigh();
                        LED3G_SetHigh();
                        LED3B_SetHigh();
#endif
                    }
                    i_boot_flag=0;
                    
                    if((CAN_RX_DATA[0]>>6)&0X01) 
                    {
                        WINKER_SetHigh();
                        IG_SetHigh();
                    }
                    else 
                    {
                        WINKER_SetLow();
                        IG_SetLow();
                    }
                    //100 0000
                    if((CAN_RX_DATA[0]>>2)&0X01)
                    {
                        LED3B_SetHigh();
                    }
                    else 
                    {
                        LED3B_SetLow();
                    }
                    if((CAN_RX_DATA[0]>>1)&0X01)
                    {
                        LED3G_SetHigh();
                    }
                    else 
                    {
                        LED3G_SetLow();
                    }
                    if((CAN_RX_DATA[0])&0X01)
                    {
                        LED3R_SetHigh();
                    }
                    else 
                    {
                        LED3R_SetLow();
                    }

                    if((CAN_RX_DATA[1]>>6)&0X01)
                    {
                        LED2B_SetHigh();
                    }
                    else
                    {
                        LED2B_SetLow();
                    }
                    if((CAN_RX_DATA[1]>>5)&0X01)
                    {
                        LED2G_SetHigh();
                    }
                    else 
                    {
                        LED2G_SetLow();
                    }
                    if((CAN_RX_DATA[1]>>4)&0X01)
                    {
                        LED2R_SetHigh();
                    }
                    else
                    {
                        LED2R_SetLow();
                    }

                    if((CAN_RX_DATA[1]>>2)&0X01)
                    {
                        // LED1B_SetHigh();
                    }
                    else
                    {
                        // LED1B_SetLow();
                    }
                    if((CAN_RX_DATA[1]>>1)&0X01)
                    {
                        LED1G_SetHigh();
                    }
                    else
                    {
                        LED1G_SetLow();
                    }
                    if((CAN_RX_DATA[1])&0X01)
                    {
                        LED1R_SetHigh();
                    }
                    else
                    {
                        LED1R_SetLow();
                    }
                }
                
                if(CAN_RX_ID == 0x124)
                {                    
                    if (CAN_RX_DATA[2] == 0x01)
                    {
                        LED1B_SetHigh();
                    }
                    else
                    {
                        LED1B_SetLow();
                    }
                }
                
            }
            else
            {
                UART1_Write("can_error");
            }
            UART1_Write(13);
        }
        if(i_100ms_flag)
        {
            i_100ms_flag=0;                            
            i_500ms_flag++;

            GET_DATA[0]=LED1R_GetValue();
            GET_DATA[1]=LED1G_GetValue();
            GET_DATA[2]=LED1B_GetValue();
            GET_DATA[3]=LED2R_GetValue();
            GET_DATA[4]=LED2G_GetValue();
            GET_DATA[5]=LED2B_GetValue();
            GET_DATA[6]=LED3R_GetValue();
            GET_DATA[7]=LED3G_GetValue();
            GET_DATA[8]=LED3B_GetValue();
            GET_DATA[9]=KEY_GetValue();
            GET_DATA[11]=EM_GetValue();
            GET_DATA[10]=(i_START_COUNT < 19 ? 1 : 0);
            if(START_GetValue()==0)
            {
                if(i_START_COUNT < 20)
                {
                    i_START_COUNT++;
                }
            }
            else 
            {
                i_START_COUNT=0;
            }
            CAN_ID = 0xC8;
            CAN_TX_DATA[0]=GET_DATA[0];
            CAN_TX_DATA[1]=GET_DATA[1];
            CAN_TX_DATA[2]=GET_DATA[2];
            CAN_TX_DATA[3]=GET_DATA[3];
            CAN_TX_DATA[4]=GET_DATA[4];
            CAN_TX_DATA[5]=GET_DATA[5];
            CAN_TX_DATA[6]=GET_DATA[6];
            CAN_TX_DATA[7]=GET_DATA[7];
            CAN_Transmit();
            CAN_ID = 0xC9;
            CAN_TX_DATA[0]=GET_DATA[8];
            CAN_TX_DATA[1]=GET_DATA[9];
            CAN_TX_DATA[2]=GET_DATA[10];
            CAN_TX_DATA[3]=GET_DATA[11];
            CAN_TX_DATA[4]=i_boot_flag;
            CAN_TX_DATA[5]=i_500ms_flag;                                    
            CAN_TX_DATA[6]=0;
            CAN_TX_DATA[7]=0;
            CAN_TX_DATA[7]=CAN_TX_DATA[7]+(ABS_GET_DATA[0]<<7)
            +(ABS_GET_DATA[1]<<6)
            +(ABS_GET_DATA[2]<<5)
            +(ABS_GET_DATA[3]<<4)
            +(ABS_GET_DATA[4]<<3)
            +(ABS_GET_DATA[5]<<2)
            +(ABS_GET_DATA[6]<<1)
            +(ABS_GET_DATA[7]);
            CAN_Transmit();
            ABS_CLK_SetLow();
            i_10us_flag=0;
            TMR1_Start();
        }
    }
    return 1; 
}
