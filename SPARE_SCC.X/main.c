/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.167.0
        Device            :  dsPIC33EV256GM106
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.50
        MPLAB 	          :  MPLAB X v5.35
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "mcc_generated_files/system.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/can1.h"
#include "mcc_generated_files/can_types.h"
#include "rs485.h"
#include "gear_control.h"
#include "string.h"

typedef uint8_t BOOL;
#define FALSE (0)
#define TRUE (1)

typedef struct {
    uint16_t s_tick;
    uint32_t CAN_ID;
    uint8_t CAN_TX_DATA[8];
    uint32_t connection_timeout_count;
} app_prm_t;

typedef enum {
    NORMAL = 0,
    ABNORMAL = 1
} device_state_t;

enum {
    SCC_ENABLE_CAN_ID = 0x52,
    SCC_DISABLE_CAN_ID = 0x53,
    SCC_COMMAND_CAN_ID = 0x62,
    GEAR_POS_CAN_ID = 0x111,
    GEAR_CMD_CAN_ID = 0x130,
    VEHICLE_SPEED_CAN_ID = 0x386,
    AEB_38D_ID = 0x38D,
    SCC_420_ID = 0x420,
    SCC_421_ID = 0x421,
    SCC_50A_ID = 0x50A
};

void T1Interrupt(void);
void CAN1_RxBuffer(void);
void Delayus(int delay);
uint8_t SccCheckSumCal(uint8_t* data, unsigned int counter);

unsigned int SCC_ID420_MainMode = 0b1;                 // bit length 1
unsigned int SCC_ID420_InfoDisplay = 0;                // bit length 3
unsigned int SCC_ID420_AliveCounter = 0;               // bit length 4 
unsigned int SCC_ID420_VsetDis = 0x3C;                 // bit length 8
unsigned int SCC_ID420_ObjValid = 0x00;                // bit length 1
unsigned int SCC_ID420_DriverAlertDisplay = 0x00;      // bit length 2
unsigned int SCC_ID420_TauGapSet = 0x04;               // bit length 3
unsigned int SCC_ID420_ObjectStatus = 0x00;            // bit length 2
unsigned int SCC_ID420_ObjectLatPos = 0x100;           // bit length 9
unsigned int SCC_ID420_ObjectDis = 0x450;              // bit length 11
unsigned int SCC_ID420_ObjectRelSpd = 0x6A4;           // bit length 12
unsigned int SCC_ID420_Camera = 0x02;                  // bit length 2

unsigned int SCC_ID421_SccMode = 0x01;                 // bit length 2
unsigned int SCC_ID421_StopReq = 0x00;                 // bit length 1
unsigned int SCC_ID421_AreQraw = 0x3FF;                // bit length 11
unsigned int SCC_ID421_AreQvalue = 0x3FF;              // bit length 11
unsigned int SCC_ID421_AliveCounter = (0x0 & 0b1111);  // bit length 4
unsigned int SCC_ID421_Checksum = (0x0 & 0b1111);

unsigned int SCC_ID389_BaudHig = 0x3F;                 // bit length 6
unsigned int SCC_ID389_BaudLow = 0x00;                 // bit length 6
unsigned int SCC_ID389_JerkMax = 0x7F;                 // bit length 7
unsigned int SCC_ID389_JerkMin = 0x7F;                 // bit length 7
unsigned int SCC_ID389_Mode2 = 0x01;                   // bit length 3

unsigned char AEB_ID38D_CF_VSM_Prefill = 0x00;         // bit length 1
unsigned char AEB_ID38D_CF_VSM_HBACmd = 0x00;          // bit length 2
unsigned char AEB_ID38D_CF_VSM_Warn = 0x00;            // bit length 2
unsigned char AEB_ID38D_CF_VSM_BeltCmd = 0x00;         // bit length 3
unsigned char AEB_ID38D_CR_VSM_DeCmd = 0x00;           // bit length 8
unsigned char AEB_ID38D_AEB_Status = 0x02;             // bit length 2 (0: not applied, 1: system off, 2: system on, 3: invalid)
unsigned char AEB_ID38D_AEB_CmdAct = 0x00;             // bit length 1 (0: no request, 1 : request valid)
unsigned char AEB_ID38D_AEB_StopReq = 0x00;            // bit length 1 (0: no request, 1: stop control is required)
unsigned char AEB_ID38D_AEB_DvrSetStatus = 0x01;       // bit length 3 (0: late, 1: normal, 2: early, 345: reserved, 6:not applied, 7:invalid)
unsigned char AEB_ID38D_AEB_FuncAct = 0x00;            // bit length 3 (4:Full brake up to 0.8g))
unsigned char AEB_ID38D_CF_VSM_DecCmdAct = 0x00;       // bit length 1
unsigned char AEB_ID38D_AEB_Failinfo = 0x00;           // bit length 3
unsigned char AEB_ID38D_CR_AEB_Alive = 0x00;           // bit length 4
unsigned char AEB_ID38D_CR_AEB_ChkSum = 0x00;          // bit length 4

unsigned char SPARE_ID1CD_AliveCounter = 0x00;

volatile app_prm_t g_app_info = {
    0,
    0,
    {0},
    0
};

device_state_t device_state = NORMAL;
gear_pos_t gear_pos = UN_DEFINED;
gear_command_t gear_cmd = NONE_CMD;

/*
                         Main application
 */
int main(void)
{
    CAN_MSG_OBJ can_recv_msg;
    CAN_MSG_OBJ can_send_msg;
    uint8_t crc_421 = 0x00;
    uint8_t crc_38D = 0x00;
    uint8_t aeb_level = 0;
    const uint32_t connection_timeout_max = 10; // 10 * 10ms = 100ms
    const uint16_t accel_offset = 400; // 400 * 0.01 = -4m/s^2
    uint8_t send_msg_data[8] = {0x00};
    uint8_t recv_msg_data[8] = {0x00};
    rs485_control_command_t rs485_cmd = NOTHING;
    BOOL connection_init = FALSE;
    
    // Initialize buffer for can message
    can_send_msg.data = send_msg_data;
    can_recv_msg.data = recv_msg_data;
    
    // Initialize the device
    SYSTEM_Initialize();
    
    // Enable CAN Transmit
    CAN1_TransmitEnable();
    
    // Enable CAN Receive
    CAN1_ReceiveEnable();
    
    // Set the CAN interrupt callback
    CAN1_SetRxBufferInterruptHandler(&CAN1_RxBuffer);
    
    // Set the can configuration mode
    CAN1_OperationModeSet(CAN_CONFIGURATION_MODE);
    
    // Set the TIMER1 interrupt callback
    TMR1_SetInterruptHandler(&T1Interrupt);
    
    // Start the TIMER1
    TMR1_Start();
    
    // Pull down to receive RS485 command
    IO_RA7_SetLow();

    // Wait CAN is ready
    while(CAN_CONFIGURATION_MODE != CAN1_OperationModeGet());
    while(CAN_OP_MODE_REQUEST_SUCCESS != CAN1_OperationModeSet(CAN_NORMAL_2_0_MODE));
    
    while (1)
    {
        // Add your application code
        switch (device_state)
        {
            case NORMAL:
                if(CAN1_ReceivedMessageCountGet() > 0) 
                {
                    if (CAN1_Receive(&can_recv_msg))
                    {
                        if (can_recv_msg.msgId == GEAR_POS_CAN_ID)
                        {
                            switch (can_recv_msg.data[1] & 0x0F) 
                            {
                                case 0x00:
                                    gear_pos = PARKING_POS;
                                    break;
                                case 0x05:
                                    gear_pos = DRIVING_POS;
                                    break;
                                case 0x06:
                                    gear_pos = NEUTRAL_POS;
                                    break;
                                case 0x07:
                                    gear_pos = REVERSE_POS;
                                    break;
                            }
                        }
                        
                        if (can_recv_msg.msgId == GEAR_CMD_CAN_ID)
                        {
                            switch (can_recv_msg.data[1] & 0x0F) 
                            {
                                case 0x00:
                                    gear_cmd = PARKING_CMD;
                                    break;
                                case 0x05:
                                    gear_cmd = DRIVING_CMD;
                                    break;
                                default:
                                    break;
                            }
                        }
                        
                        if (can_recv_msg.msgId == AEB_38D_ID && can_recv_msg.field.dlc == 8)
                        {
                            AEB_ID38D_CR_AEB_Alive = can_recv_msg.data[7] & 0x0F;
                            g_app_info.connection_timeout_count = 0;
                        }
                        else if (can_recv_msg.msgId == SCC_421_ID && can_recv_msg.field.dlc == 8)
                        {
                            SCC_ID421_AliveCounter = can_recv_msg.data[7] & 0x0F;
                            g_app_info.connection_timeout_count = 0;
                        }
                        else if (can_recv_msg.msgId == SCC_420_ID || can_recv_msg.msgId == SCC_50A_ID)
                        {
                            g_app_info.connection_timeout_count = 0;
                        }
                        else if (can_recv_msg.msgId == SCC_ENABLE_CAN_ID
                                || can_recv_msg.msgId == SCC_DISABLE_CAN_ID
                                || can_recv_msg.msgId == SCC_COMMAND_CAN_ID)
                        {
                            //g_app_info.connection_timeout_count = 0;
                            connection_init = TRUE;
                        }
                    }
                }
                
                if (g_app_info.connection_timeout_count > connection_timeout_max && connection_init)
                {
                    g_app_info.connection_timeout_count = connection_timeout_max + 1;
                    device_state = ABNORMAL;
                    connection_init = FALSE;
                    // Pull high to send RS485 command
                    // IO_RA7_SetHigh();
                }
                
                // Every 50 ms
                if (g_app_info.s_tick >= 5)
                {
                    // Check the rs485 command from XAVIER
                    g_app_info.s_tick = 0;
                    rs485_cmd = ReceiveRs485Command();
                    
                    if (rs485_cmd == GEAR_P)
                    {
                        gear_cmd = PARKING_CMD;
                    }
                    else if (rs485_cmd == GEAR_D)
                    {
                        gear_cmd = DRIVING_CMD;
                    }
                    
                    SendGearPos(gear_pos, gear_cmd);
                    
                    // Send the status message through CCAN to XAVIER
                    // Alive Counter for SCC PID 1CC
                    if (SPARE_ID1CD_AliveCounter > 0xE)
                    {
                      SPARE_ID1CD_AliveCounter = 0;
                    }
                    else
                    {
                      SPARE_ID1CD_AliveCounter ++;
                    }
                    can_send_msg.msgId = 0x1CD;
                    can_send_msg.field.idType = 0;
                    can_send_msg.field.frameType = 0;
                    can_send_msg.field.dlc = 8;
                    memset(can_send_msg.data, 0x00, 8);
                    can_send_msg.data[0] = 0x01;
                    can_send_msg.data[1] = 0x00;
                    can_send_msg.data[2] = 0x00;
                    can_send_msg.data[3] = 0x00;
                    can_send_msg.data[4] = 0x00;
                    can_send_msg.data[5] = 0x00;
                    can_send_msg.data[6] = 0x00;
                    can_send_msg.data[7] = SPARE_ID1CD_AliveCounter;
                    CAN1_Transmit(CAN_PRIORITY_HIGH, &can_send_msg);
                    Delayus(100);
                }
                
                Delayus(100);
                break;
            case ABNORMAL:
                if (g_app_info.s_tick % 2 == 0)
                {   
                    // Alive Counter for SCC PID 420
                    if (SCC_ID420_AliveCounter > 0xE)
                    {
                      SCC_ID420_AliveCounter = 0;
                    }
                    else
                    {
                      SCC_ID420_AliveCounter ++;
                    }
                    
                    // Alive Counter for SCC PID 421
                    if (SCC_ID421_AliveCounter > 0xD)
                    {
                      SCC_ID421_AliveCounter = 0;
                    }
                    else
                    {
                      SCC_ID421_AliveCounter ++;
                    }
                    
                    // Alive Counter for AEB
                    if (AEB_ID38D_CR_AEB_Alive > 0x0D)
                    {
                        AEB_ID38D_CR_AEB_Alive = 0;
                    }
                    else
                    {
                        AEB_ID38D_CR_AEB_Alive ++;
                    }
                    
                    // Sending PID_420 message
                    can_send_msg.msgId = 0x420;
                    can_send_msg.field.idType = 0;
                    can_send_msg.field.frameType = 0;
                    can_send_msg.field.dlc = 8;
                    memset(can_send_msg.data, 0x00, 8);
                    can_send_msg.data[0] = (SCC_ID420_AliveCounter << 4) | ((SCC_ID420_InfoDisplay << 3) | SCC_ID420_MainMode);
                    can_send_msg.data[1] = SCC_ID420_VsetDis;
                    can_send_msg.data[2] = (SCC_ID420_ObjectStatus << 6) | (SCC_ID420_TauGapSet << 3) | (SCC_ID420_DriverAlertDisplay << 1) | SCC_ID420_ObjValid;
                    can_send_msg.data[3] = (SCC_ID420_ObjectLatPos & 0xFF);
                    can_send_msg.data[4] = ((SCC_ID420_ObjectDis & 0x07F) << 1) | ((SCC_ID420_ObjectLatPos & 0x100) >> 8);
                    can_send_msg.data[5] = ((SCC_ID420_ObjectRelSpd & 0x00F) << 4) | ((SCC_ID420_ObjectDis & 0x780) >> 7);
                    can_send_msg.data[6] = ((SCC_ID420_ObjectRelSpd & 0xFF0) >> 4);
                    can_send_msg.data[7] = SCC_ID420_Camera << 6;
                    CAN1_Transmit(CAN_PRIORITY_HIGH, &can_send_msg);
                    Delayus(100);
                    
                    // Sending PID_421 message
                    can_send_msg.msgId = 0x421;
                    can_send_msg.field.idType = 0;
                    can_send_msg.field.frameType = 0;
                    can_send_msg.field.dlc = 8;
                    memset(can_send_msg.data, 0x00, 8);
                    can_send_msg.data[0] = 0x00;
                    can_send_msg.data[1] = (SCC_ID421_StopReq << 7) | (SCC_ID421_SccMode << 5);
                    can_send_msg.data[2] = 0x00;
                    can_send_msg.data[3] = ((SCC_ID421_AreQraw - accel_offset) & 0x0FF);
                    can_send_msg.data[4] = (((SCC_ID421_AreQvalue - accel_offset) & 0x007) << 5) | (((SCC_ID421_AreQraw - accel_offset) & 0x700) >> 8);
                    can_send_msg.data[5] = ((SCC_ID421_AreQvalue - accel_offset) & 0x7F8) >> 3;  
                    can_send_msg.data[6] = 0x00;
                    crc_421 = SccCheckSumCal((uint8_t *)can_send_msg.data, SCC_ID421_AliveCounter);
                    can_send_msg.data[7] = (crc_421 << 4) | SCC_ID421_AliveCounter;   
                    CAN1_Transmit(CAN_PRIORITY_HIGH, &can_send_msg);
                    Delayus(100);
                    
                    // Sending PID_0x38D message
                    can_send_msg.msgId = 0x38D;
                    can_send_msg.field.idType = 0;
                    can_send_msg.field.frameType = 0;
                    can_send_msg.field.dlc = 8;
                    memset(can_send_msg.data, 0x00, 8);

                    if (aeb_level == 1)
                    {
                        can_send_msg.data[0] = 0x56;
                        can_send_msg.data[1] = 0x14;
                    }
                    else if (aeb_level == 2)
                    {
                        can_send_msg.data[0] = 0x3E;
                        can_send_msg.data[1] = 0x63;
                    }
                    else
                    {
                        can_send_msg.data[0] = AEB_ID38D_CF_VSM_Prefill | AEB_ID38D_CF_VSM_HBACmd | AEB_ID38D_CF_VSM_Warn | AEB_ID38D_CF_VSM_BeltCmd;
                        can_send_msg.data[1] = AEB_ID38D_CR_VSM_DeCmd;
                    }
                    
                    can_send_msg.data[2] = ((AEB_ID38D_AEB_Status << 2) & 0xFF) | ((AEB_ID38D_AEB_CmdAct << 4) & 0xFF) | 
                                            ((AEB_ID38D_AEB_StopReq << 5) & 0xFF) | ((AEB_ID38D_AEB_DvrSetStatus << 6) & 0xFF);
                    can_send_msg.data[3] = ((AEB_ID38D_AEB_DvrSetStatus >> 2) & 0xFF) | ((AEB_ID38D_AEB_FuncAct << 2) & 0xFF) |
                                            ((AEB_ID38D_CF_VSM_DecCmdAct << 7) & 0xFF);
                    can_send_msg.data[4] = (AEB_ID38D_AEB_Failinfo & 0xFF);
                    can_send_msg.data[5] = 0x00;
                    can_send_msg.data[6] = 0x00;
                    crc_38D = SccCheckSumCal((uint8_t *)can_send_msg.data, AEB_ID38D_CR_AEB_Alive);
                    can_send_msg.data[7] = (crc_38D << 4) | AEB_ID38D_CR_AEB_Alive;
                    CAN1_Transmit(CAN_PRIORITY_HIGH, &can_send_msg);
                    Delayus(100);
                }
                
                if (g_app_info.s_tick > 50)
                {
                    g_app_info.s_tick = 0;
                    // Send the command to active Brake actuator via RS485
                    SendBrakeAngle(1, 80, 0x20);
                    // IO_RA12_Toggle();
                }
                
                if (g_app_info.s_tick % 5 == 0)
                {
                    if(CAN1_ReceivedMessageCountGet() > 0) 
                    {
                        if (CAN1_Receive(&can_recv_msg))
                        {
                            if (can_recv_msg.msgId == SCC_50A_ID)
                            {
                                g_app_info.connection_timeout_count = 0;
                                // Release the brake
                                SendBrakeAngle(1, 0, 0x20);
                                Delayus(10000);
                                SendBrakeAngle(1, 0, 0x20);
                                Delayus(10000);
                                SendBrakeAngle(1, 0, 0x20);
                                Delayus(10000);
                                device_state = NORMAL;
                                // Pull low to receive RS485 command
                                IO_RA7_SetLow();
                            }
                        }
                    }
                }
                Delayus(100);
                break;
            default:
                break;
        }
    }
    
    return 1; 
}

uint8_t SccCheckSumCal(uint8_t* data, unsigned int counter)
{
    uint8_t crc = 0x00;

    crc = 0x10 - (((data[0] >> 4) + (data[0] & 0b1111)
    +(data[1] >> 4) + (data[1] & 0b1111)
    +(data[2] >> 4) + (data[2] & 0b1111)
    +(data[3] >> 4) + (data[3] & 0b1111)
    +(data[4] >> 4) + (data[4] & 0b1111)
    +(data[5] >> 4) + (data[5] & 0b1111)
    +(data[6] >> 4) + (data[6] & 0b1111)
    + counter) & 0b1111);

    return crc;
}

void T1Interrupt(void)
{
    g_app_info.s_tick ++;
    g_app_info.connection_timeout_count ++;
}

void CAN1_RxBuffer(void)
{
    // CAN1 Receive buffer application code
}

void Delayus(int delay)
{
    int i;
    for (i = 0; i < delay; i++)
    {
        __asm__ volatile ("repeat #39");
        __asm__ volatile ("nop");
    }
}

/**
 End of File
*/

