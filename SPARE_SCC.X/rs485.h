/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#ifndef _RS485_H
#define _RS485_H

#include "stdint.h"
#include "string.h"
#include "mcc_generated_files/uart2.h"

#define RS485_CMD_HEADER_H          0xFF
#define RS485_CMD_HEADER_L          0xF1
#define RS485_CMD_GEAR_P            0x01
#define RS485_CMD_GEAR_D            0x02

typedef struct {
  unsigned char Header_H;
  unsigned char Header_L;
  unsigned char Id;
  unsigned char Size;
  unsigned char CRC;
  unsigned char Mode;
  unsigned char Direction;
  unsigned char Degree_H;
  unsigned char Degree_L;
  unsigned char Rpm_H;
  unsigned char Rpm_L;
} rs485_motor_command_t;

typedef enum {
  NOTHING = 0,
  GEAR_P = 1,
  GEAR_D = 2,
} rs485_control_command_t;

typedef enum {
  HEADER = 0,
  DATA = 1,
} rs485_receive_state_t;

void SendBrakeAngle(uint8_t direction, uint8_t angle, uint8_t rpm);
rs485_control_command_t ReceiveRs485Command(void);

#endif  //_RS485_H
/**
 End of File
*/