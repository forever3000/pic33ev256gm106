/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "gear_control.h"
#include "mcc_generated_files/pin_manager.h"

gear_operation_state_t gear_operation_state = GEAR_NORMAL;

void SendGearPos(gear_pos_t curr_pos, gear_command_t cmd)
{
    static uint16_t stuck_cnt = 0;
    static uint16_t abnormal_cnt = 0;
    const uint16_t max_stuck_cnt = 40; // 4s
    
    if (stuck_cnt > max_stuck_cnt)
    {
        gear_operation_state = GEAR_ABNORMAL;
        stuck_cnt = 0;
    }
    
    switch (gear_operation_state)
    {
        case GEAR_NORMAL:
            switch (curr_pos)
            {
                case PARKING_POS:
                    if (cmd == DRIVING_CMD)
                    {
                        IO_RA11_SetHigh();
                        IO_RA12_SetLow();
                    }
                    else if (cmd == PARKING_CMD)
                    {
                        IO_RA11_SetLow();
                        IO_RA12_SetLow();
                    }
                    stuck_cnt = 0;
                    break;
                case REVERSE_POS:
                    if (stuck_cnt > max_stuck_cnt / 2)
                    {
                        IO_RA11_SetLow();
                        IO_RA12_SetLow();
                    }
                    stuck_cnt ++;
                    break;
                case NEUTRAL_POS:
                    if (stuck_cnt > max_stuck_cnt / 2)
                    {
                        IO_RA11_SetLow();
                        IO_RA12_SetLow();
                    }
                    stuck_cnt ++;
                    break;
                case DRIVING_POS:
                    if (cmd == DRIVING_CMD)
                    {
                        IO_RA11_SetLow();
                        IO_RA12_SetLow();
                    }
                    else if (cmd == PARKING_CMD)
                    {
                        IO_RA11_SetLow();
                        IO_RA12_SetHigh();
                    }
                    stuck_cnt = 0;
                    break;
            }
            break;
        case GEAR_ABNORMAL:
            switch (curr_pos)
            {
                case PARKING_POS:
                case DRIVING_POS:
                    IO_RA11_SetLow();
                    IO_RA12_SetLow();
                    abnormal_cnt ++;
                    if (abnormal_cnt > 20)
                    {
                        gear_operation_state = GEAR_NORMAL;
                        abnormal_cnt = 0;
                    }
                    break;
                case REVERSE_POS:
                case NEUTRAL_POS:
                    IO_RA11_SetHigh();
                    IO_RA12_SetLow();
                    break;
            }
            break;
        default:
            break;
    }
}