/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "rs485.h"

rs485_receive_state_t rs485_receive_state = HEADER;

// For the brake motor
unsigned char CalCRC8(rs485_motor_command_t cmd)
{
    unsigned char crc = 0;
    uint8_t i = 0;
    const uint8_t* data = (uint8_t*) &cmd.Mode;

    crc = crc + cmd.Id + cmd.Size;

    for (i = 0; i < cmd.Size - 1; ++i)
    {
        crc = crc + data[i];
    }

  return ~crc;
}

void SendBrakeAngle(uint8_t direction, uint8_t angle, uint8_t rpm)
{
    rs485_motor_command_t motor_command;
    
    motor_command.Header_H = 0xFF;
    motor_command.Header_L = 0xFE;
    motor_command.Id= 0x00;
    motor_command.Size = 0x07;
    motor_command.Mode = 0x01;
    motor_command.Direction = direction;
    motor_command.Degree_H = ((angle * 100) >> 8) & 0xFF;
    motor_command.Degree_L = (angle * 100) & 0xFF;
    motor_command.Rpm_H = ((rpm * 10) >> 8) & 0xFF;
    motor_command.Rpm_L = (rpm * 10) & 0xFF;
    motor_command.CRC = CalCRC8(motor_command);
    
    if (UART2_IsTxReady())
    {
        UART2_WriteBuffer((&motor_command.Header_H) ,sizeof(motor_command));
    }
}

rs485_control_command_t ReceiveRs485Command(void)
{
    uint8_t buffer[1];
    uint8_t rx_count = 0;
    rs485_control_command_t ret = NOTHING;
    uint8_t crc = 0x00;
    
    switch (rs485_receive_state)
    {
        case HEADER:
            rx_count = UART2_ReadBuffer(buffer, 1);
            if (rx_count)
            {
                if (buffer[0] == RS485_CMD_HEADER_H)
                {
                    rx_count = 0;
                    rx_count = UART2_ReadBuffer(buffer, 1);
                    if (rx_count)
                    {
                        if (buffer[0] == RS485_CMD_HEADER_L)
                        {
                            rs485_receive_state = DATA;
                        }
                    }
                }
            }
            break;
        case DATA:
            rx_count = 0;
            rx_count = UART2_ReadBuffer(buffer, 1);
            if (rx_count)
            {
                if (buffer[0] == RS485_CMD_GEAR_P)
                {
                    crc = buffer[0];

                    rx_count = UART2_ReadBuffer(buffer, 1);
                    if (rx_count)
                    {
                        if (buffer[0] != crc)
                        {
                            ret = NOTHING;
                        }
                        else
                        {
                            ret = GEAR_P;
                        }
                    }
                    else
                    {
                        ret = NOTHING;
                    }

                    rs485_receive_state = HEADER;
                }
                else if (buffer[0] == RS485_CMD_GEAR_D)
                {
                    crc = buffer[0];
                    ret = GEAR_D;

                    rx_count = UART2_ReadBuffer(buffer, 1);
                    if (rx_count)
                    {
                        if (buffer[0] != crc)
                        {
                            ret = NOTHING;
                        }
                        else
                        {
                            ret = GEAR_D;
                        }
                    }
                    else
                    {
                        ret = NOTHING;
                    }

                    rs485_receive_state = HEADER;
                }
                else
                {
                    ret = NOTHING;
                    rs485_receive_state = HEADER;
                }
            }
            else
            {
                rs485_receive_state = HEADER;
            }
            break;
        default:
            break;
    }
    
    return ret;
}