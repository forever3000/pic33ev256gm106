/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "epb.h"

void EnableEpb(uint16_t interval)
{
    CAN_MSG_OBJ can_send_msg;
    uint8_t send_msg_data[8] = {0x00};
    
    can_send_msg.data = send_msg_data;
    
    can_send_msg.msgId = 0x50B;
    can_send_msg.field.idType = 0;
    can_send_msg.field.frameType = 0;
    can_send_msg.field.dlc = 8;
    memset(can_send_msg.data, 0x00, 8);
    can_send_msg.data[0] = 0x00;
    can_send_msg.data[1] = 0x00;
    can_send_msg.data[2] = 0x00;
    can_send_msg.data[3] = 0x00;
    can_send_msg.data[4] = 0x00;
    can_send_msg.data[5] = 0x00;
    can_send_msg.data[6] = 0x00;
    can_send_msg.data[7] = 0xFF;
    CAN1_Transmit(CAN_PRIORITY_HIGH, &can_send_msg);
    Delayus(100);
}

void DisableEpb(uint16_t interval)
{
    CAN_MSG_OBJ can_send_msg;
    uint8_t send_msg_data[8] = {0x00};
    
    can_send_msg.data = send_msg_data;
    
    can_send_msg.msgId = 0x50C;
    can_send_msg.field.idType = 0;
    can_send_msg.field.frameType = 0;
    can_send_msg.field.dlc = 8;
    memset(can_send_msg.data, 0x00, 8);
    can_send_msg.data[0] = 0x00;
    can_send_msg.data[1] = 0x00;
    can_send_msg.data[2] = 0x00;
    can_send_msg.data[3] = 0x00;
    can_send_msg.data[4] = 0x00;
    can_send_msg.data[5] = 0x00;
    can_send_msg.data[6] = 0x00;
    can_send_msg.data[7] = 0xFF;
    CAN1_Transmit(CAN_PRIORITY_HIGH, &can_send_msg);
    Delayus(100);
}