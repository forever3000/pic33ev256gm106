
//#include <Wire.h>
#include <SPI.h>
#include <CAN.h>

 

int can_read_cnt = 0;
byte can_read_data[8];

const int MPU_addr = 0x49; //now MPU addr is not adapted

const int spoof_en_addr = 6; // PD6
const int cs_a_b_addr = 9;  // mcp4922 , dac
const int cs_b_b_addr = 10; // mcp2515 , com
const int tp35_addr = 7;
//pin assign

int sensorPin1 = A0;    // select the input pin for the potentiometer
int sensorPin2 = A1;    // select the input pin for the potentiometer
int sensorPin3 = A2;    // select the input pin for the potentiometer
int sensorPin4 = A3;    // select the input pin for the potentiometer

float sensorValue1 = 0;  // variable to store the value coming from the sensor
float sensorValue2 = 0;  // variable to store the value coming from the sensor
float sensorValue3 = 0;  // variable to store the value coming from the sensor
float sensorValue4 = 0;  // variable to store the value coming from the sensor

uint16_t steering_center_var = 2000;
uint16_t steering_test_var = 0;
uint16_t steering_send_high = steering_center_var + steering_test_var;
uint16_t steering_send_low = steering_center_var - steering_test_var;

uint16_t steering_send_high2 = steering_send_high % 256;
uint16_t steering_send_high3 = steering_send_high / 256;
uint16_t steering_send_low4 = steering_send_high % 256;
uint16_t steering_send_low5 = steering_send_high / 256;

int adc_loop_en = 0;

const int mcp_cha_reg = 0x0F;
const int mcp_chb_reg = 0x07;

int dac_code_cha1 = 0x00;
int dac_code_cha2 = 0x00;

int dac_code_chb1 = 0x00;
int dac_code_chb2 = 0x00;

char cTemp; // complete command
String sCommand = "";
String sTemp = "";
//serial communication variable


void dac_init()
{
  digitalWrite(cs_a_b_addr,LOW);
  delayMicroseconds(100);
      
  SPI.transfer((0x7<<4 | 0x00));
  SPI.transfer(0x00);
  //Serial.println(0xFF,BIN);
  delayMicroseconds(100);

  digitalWrite(cs_a_b_addr,HIGH);
  delayMicroseconds(100);

  digitalWrite(cs_a_b_addr,LOW);
  delayMicroseconds(100);
      
  SPI.transfer((0xF<<4 | (0x00) ));
  SPI.transfer(((0x00)));
  //Serial.println(0xFF,BIN);
  delayMicroseconds(100);
      
  digitalWrite(cs_a_b_addr,HIGH);
  delayMicroseconds(100);
}

void can_send_command_func(int cid, byte data_0, byte data_1, byte data_2, byte data_3,
                                    byte data_4, byte data_5, byte data_6, byte data_7)
{

  Serial.print(cid,HEX);
  Serial.print(" ");
  Serial.print(data_0,HEX);
  Serial.print(" ");
  Serial.print(data_1,HEX);
  Serial.print(" ");
  Serial.print(data_2,HEX);
  Serial.print(" ");
  Serial.print(data_3,HEX);
  Serial.print(" ");
  Serial.print(data_4,HEX);
  Serial.print(" ");
  Serial.print(data_5,HEX);
  Serial.print(" ");
  Serial.print(data_6,HEX);
  Serial.print(" ");
  Serial.print(data_7,HEX);
  Serial.println("");
 
//if(data_0 > 0xFF) data_0 = 0xFF;
  CAN.beginPacket(cid);
  CAN.write(data_0);
  CAN.write(data_1);
  CAN.write(data_2);
  CAN.write(data_3);
  CAN.write(data_4);
  CAN.write(data_5);
  CAN.write(data_6);
  CAN.write(data_7);
  CAN.endPacket();

  //Serial.println("can-transfer done");
  //delay(100);
}
void can_reiceve_read_basic_func() {
  int packetSize = CAN.parsePacket();
    if(packetSize)
  {
    //Serial.print("CAN - Receiver ");
    if(CAN.packetExtended())
    {
      Serial.print("extended ");
    }
    if(CAN.packetRtr())
    {
      Serial.print("RTR ");
    }
    //Serial.print("packet with id 0x ");
    
    if(CAN.packetId() < 0x500)
    {
      //if(CAN.packetId() == 0x4B0)
      {
        
    Serial.print(CAN.packetId(),HEX);
    Serial.print(" :\t ");
      if(CAN.packetRtr())
      {
        //Serial.print(" and requested length ");
        //Serial.println(CAN.packetDlc());
      }
      else
      {
        //Serial.print(" and length ");
        //Serial.println(packetSize);
  
        while(CAN.available())
        {
          //Serial.print(can_read_cnt);
          //Serial.print(" : ");
  
          can_read_data[can_read_cnt] = (char)CAN.read();
          if(can_read_data[can_read_cnt] < 0) 
          {
            can_read_data[can_read_cnt] = can_read_data[can_read_cnt] + 256;
          }
          Serial.print(can_read_data[can_read_cnt],HEX);
          //Serial.print(" , ");
          Serial.print("  ");
          can_read_cnt++;
          if((can_read_cnt > 7) || (can_read_cnt < 0)) can_read_cnt = 0;
      }
      Serial.println();
    }
  }
    }
  } //if(packetSize)

}
byte wheel_value[2];
void can_reiceve_wheel_func() {
  int packetSize = CAN.parsePacket();
    if(packetSize)
  {
    //Serial.print("CAN - Receiver ");
    if(CAN.packetExtended())
    {
      Serial.print("extended ");
    }
    if(CAN.packetRtr())
    {
      Serial.print("RTR ");
    }
    //Serial.print("packet with id 0x ");
    //if((CAN.packetId() > 0x300) && (CAN.packetId() < 0x370))
    if((CAN.packetId() == 0x2B0))
    {
    Serial.print(CAN.packetId(),HEX);
    Serial.print(" ");
    if(CAN.packetRtr())
    {
      //Serial.print(" and requested length ");
      //Serial.println(CAN.packetDlc());
    }
    else
    {
      //Serial.print(" and length ");
      //Serial.println(packetSize);

      while(CAN.available())
      {
        //Serial.print(can_read_cnt);
        //Serial.print(" : ");

        can_read_data[can_read_cnt] = (char)CAN.read();




        Serial.print(can_read_data[can_read_cnt],HEX);
        //Serial.print(" , ");
        Serial.print("  ");
        can_read_cnt++;
        if((can_read_cnt > 7) || (can_read_cnt < 0)) {
          wheel_value[1] = can_read_data[0];
          wheel_value[0] = can_read_data[1];

          can_read_cnt = 0;
        }
      } 
        
        
      Serial.println();
      Serial.print(wheel_value[0] & 0xFF,HEX);
      Serial.print(" ");
      Serial.println(wheel_value[1] & 0xFF,HEX);
    }
  } //if(packetSize)
    }
//delay(100);

}
void can_reiceve_read_func() {
  int packetSize = CAN.parsePacket();
  if(CAN.packetId() == 0x394) {
    if(packetSize)
  {
    Serial.print("CAN - Receiver ");
    if(CAN.packetExtended())
    {
      Serial.print("extended ");
    }
    if(CAN.packetRtr())
    {
      Serial.print("RTR ");
    }
    Serial.print("packet with id 0x ");
    Serial.print(CAN.packetId(),HEX);
    if(CAN.packetRtr())
    {
      Serial.print(" and requested length ");
      Serial.println(CAN.packetDlc());
    }
    else
    {
      Serial.print(" and length ");
      Serial.println(packetSize);

      while(CAN.available())
      {
        Serial.print(can_read_cnt);
        Serial.print(" : ");

        can_read_data[can_read_cnt] = (char)CAN.read();
        if(can_read_data[can_read_cnt] < 0) {
          can_read_data[can_read_cnt] = can_read_data[can_read_cnt] + 256;
        }
        //Serial.print(can_read_data[can_read_cnt]);
        //Serial.print(" , ");
        Serial.print("  ");
        can_read_cnt++;
        if((can_read_cnt > 7) || (can_read_cnt < 0)) can_read_cnt = 0;
      }
      Serial.println();
    }
  } //if(packetSize)
  } //if(CAN.packetId() == 0x394)



  

  
}
void can_reiceve_func() {
  int packetSize = CAN.parsePacket();
  if(CAN.packetId() == 0xAD) {
    if(packetSize)
  {
    Serial.print("CAN - Receiver ");
    if(CAN.packetExtended())
    {
      Serial.print("extended ");
    }
    if(CAN.packetRtr())
    {
      Serial.print("RTR ");
    }
    Serial.print("packet with id 0x ");
    Serial.print(CAN.packetId(),HEX);
    if(CAN.packetRtr())
    {
      Serial.print(" and requested length ");
      Serial.println(CAN.packetDlc());
    }
    else
    {
      Serial.print(" and length ");
      Serial.println(packetSize);

      while(CAN.available())
      {
        Serial.print(can_read_cnt);
        Serial.print(" : ");

        can_read_data[can_read_cnt] = (char)CAN.read();
        if(can_read_data[can_read_cnt] < 0) {
          can_read_data[can_read_cnt] = can_read_data[can_read_cnt] + 256;
        }
        Serial.print(can_read_data[can_read_cnt]);
        Serial.print(" , ");
        can_read_cnt++;
        if((can_read_cnt > 7) || (can_read_cnt < 0)) can_read_cnt = 0;
      }
      Serial.println();
    }
  } //if(packetSize)
////////////////////////////////////////////
// can_communication action define
////////////////////////////////////////////
  if(can_read_data[0] == 0x00) {         
    if(can_read_data[2] == 0x00) {
      digitalWrite(spoof_en_addr,LOW);
      delay(100);
      Serial.println("relay state - nc");
      delay(100);
    }
  } //if(can_read_data[0] == 0x00)
  if(can_read_data[0] == 0x00) {
    if(can_read_data[2] == 0x01) {
      digitalWrite(spoof_en_addr,HIGH);
      delay(100);
      Serial.println("relay state - no");
      delay(100);
      }
  } //if(can_read_data[0] == 0x00)
  if(can_read_data[0] == 0xAD) {
    if(can_read_data[1] == 0xFF) {
      if(can_read_data[3] == 0xFF) {
        if(can_read_data[5] == 0xFF) {
          if(can_read_data[7] == 0xFF) {
              digitalWrite(cs_a_b_addr,LOW);
              delayMicroseconds(100);
                  
              SPI.transfer((0x7<<4 | can_read_data[2]));
              SPI.transfer(can_read_data[3]);
              //Serial.println(0xFF,BIN);
              delayMicroseconds(100);
            
              digitalWrite(cs_a_b_addr,HIGH);
              delayMicroseconds(100);
            
              digitalWrite(cs_a_b_addr,LOW);
              delayMicroseconds(100);
                  
              SPI.transfer((0xF<<4 | (can_read_data[2] / 2) ));
              SPI.transfer(((can_read_data[3] / 2)));
              //Serial.println(0xFF,BIN);
              delayMicroseconds(100);
                  
              digitalWrite(cs_a_b_addr,HIGH);
              delayMicroseconds(100);
          
          } //if(can_read_data[7] == 0xFF
        } //if(can_read_data[5] == 0xFF
      } //if(can_read_data[3] == 0xFF
    } //if(can_read_data[1] == 0xFF
  } //if(can_read_data[0] == 0xAD)
  
  } //if(CAN.packetId() == 0xAD)
  
}

void Serial_command_func() {
    sCommand = "";
  while(Serial.available())
  {
    cTemp = Serial.read();
    sCommand.concat(cTemp);
  }

  if(sCommand != "")
  {
    Serial.println("Input word : " + sCommand);
    //Serial.print(sCommand);
    
    char cTempData[4];
    sCommand.substring(0,2).toCharArray(cTempData,3);
    char cTempData_sub1[4];
    sCommand.substring(2,4).toCharArray(cTempData_sub1,3);
    int temp_data = atoi(cTempData_sub1);
    char cTempData_sub2[4];
    sCommand.substring(4,6).toCharArray(cTempData_sub2,3);
    int temp_data2 = atoi(cTempData_sub2); 

    int reg1 = ((temp_data*100) + (temp_data2))/1.2;
    int reg2 = reg1/256;
    int reg3 = reg1%256;
    int reg4 = reg1 ;
    int reg5 = reg4/256;
    int reg6 = reg4%256;
    
    char string_test1[] = "hi";
    char string_test2[] = "no";
    char string_test3[] = "nc";
    char string_test4[] = "da";
    char string_test5[] = "db";
    char string_test6[] = "tt";
    char string_test7[] = "sm";
    char string_test8[] = "dc";
    char string_test9[] = "cc";
    char string_test10[] = "st";
    char string_test11[] = "th";
    char string_test12[] = "ts";
    char string_test13[] = "kk";
    if(strcmp(string_test1,cTempData) == 0)
    { //hi
      Serial.println("return word : hello");
      can_send_command_func(0xaa,0x12,0x34,0x56,0x78,0x9a,0xa1,0xb1,0xc1);
      delay(100);
    }
    if(strcmp(string_test2,cTempData) == 0)
    { //no
      can_send_command_func(0x54,0x05,0xCC,0x00,0x00,0x00,0x00,0x00,0x00);
      can_send_command_func(0x52,0x05,0xCC,0x00,0x00,0x00,0x00,0x00,0x00);
      
      //digitalWrite(spoof_en_addr,HIGH); 
      //delay(100);
      Serial.println("relay state - no");
      //delay(100);
      delay(10);
      
    }
    if(strcmp(string_test3,cTempData) == 0)
    { //nc
//can_send_command_func(0x54,0x05,0xCC,0x00,0x00,0x00,0x00,0x00,0x00);
      can_send_command_func(0x55,0x05,0xCC,0x00,0x00,0x00,0x00,0x00,0x00);
      can_send_command_func(0x53,0x05,0xCC,0x00,0x00,0x00,0x00,0x00,0x00);
      //digitalWrite(spoof_en_addr,LOW);
      //delay(100);
      Serial.println("relay state - nc");
      //delay(100);
      delay(10);
    }

    if(strcmp(string_test6,cTempData) == 0)
    { //tt
      Serial.println("command : tt");

      Serial.print("adc loop en = ");
      adc_loop_en = ~adc_loop_en;
      Serial.println(adc_loop_en);
      
        
    }
        if(strcmp(string_test10,cTempData) == 0)
    { //st
    reg1 = (2500 + (temp_data*100) + (temp_data2))/1.22;
    reg2 = reg1/256;
    reg3 = reg1%256;
    reg4 = (2500 - (temp_data*100) - (temp_data2))/1.22; ;
    reg5 = reg4/256;
    reg6 = reg4%256;
    
      digitalWrite(cs_a_b_addr,LOW);
      delay(1);
      
      SPI.transfer((0x7<<4 | reg2));
      SPI.transfer(reg3);
      //Serial.println(0xFF,BIN);
      delay(1);

      digitalWrite(cs_a_b_addr,HIGH);
      delay(1);

      digitalWrite(cs_a_b_addr,LOW);
      delay(1);
      
      SPI.transfer((0xF<<4 | reg5));
      SPI.transfer(reg6);
      //Serial.println(0xFF,BIN);
      delay(1);
      
      digitalWrite(cs_a_b_addr,HIGH);
      delay(1);
      Serial.println("dac register changed");
      delay(1);
    }
        if(strcmp(string_test12,cTempData) == 0)
    { //st
    reg1 = (2500 - (temp_data*100) - (temp_data2))/1.22;
    reg2 = reg1/256;
    reg3 = reg1%256;
    reg4 = (2500 + (temp_data*100) + (temp_data2))/1.22; ;
    reg5 = reg4/256;
    reg6 = reg4%256;
    
      digitalWrite(cs_a_b_addr,LOW);
      delay(1);
      
      SPI.transfer((0x7<<4 | reg2));
      SPI.transfer(reg3);
      //Serial.println(0xFF,BIN);
      delay(1);

      digitalWrite(cs_a_b_addr,HIGH);
      delay(1);

      digitalWrite(cs_a_b_addr,LOW);
      delay(1);
      
      SPI.transfer((0xF<<4 | reg5));
      SPI.transfer(reg6);
      //Serial.println(0xFF,BIN);
      delay(1);
      
      digitalWrite(cs_a_b_addr,HIGH);
      delay(1);
      Serial.println("dac register changed");
      delay(1);
    }
        if(strcmp(string_test11,cTempData) == 0)
    { //th
    reg1 = ((temp_data*100) + (temp_data2))/1.2;
    reg2 = reg1/256;
    reg3 = reg1%256;
    reg4 = reg1/2 ;
    reg5 = reg4/256;
    reg6 = reg4%256;
      digitalWrite(cs_a_b_addr,LOW);
      delay(100);
      
      SPI.transfer((0x7<<4 | reg2));
      SPI.transfer(reg3);
      //Serial.println(0xFF,BIN);
      delay(100);

      digitalWrite(cs_a_b_addr,HIGH);
      delay(100);

      digitalWrite(cs_a_b_addr,LOW);
      delay(100);
      
      SPI.transfer((0xF<<4 | reg5));
      SPI.transfer(reg6);
      //Serial.println(0xFF,BIN);
      delay(100);
      
      digitalWrite(cs_a_b_addr,HIGH);
      delay(100);
      Serial.println("dac register changed");
      delay(100);
    }
    //delay(100);
        if(strcmp(string_test13,cTempData) == 0)
    { //kk
      can_send_command_func(0x54,0x05,0xCC,0,0,0,0,0,0);
      delay(500);
////////////////////////////////////////////////////
      long can_buf1 = 2000;
      long can_buf_sub1 = 0;
      long can_buf_high = can_buf1 + can_buf_sub1;
      long can_buf_low = can_buf1 - can_buf_sub1;
      
      byte can_buf2 = can_buf_low/256;
      byte can_buf3 = can_buf_low%256;
      byte can_buf4 = can_buf_high/256;
      byte can_buf5 = can_buf_high%256;
      Serial.print("send :  ");
      Serial.print(can_buf2,HEX);
      Serial.print(" ");
      Serial.print(can_buf3,HEX);
      Serial.print(" ");
      Serial.print(can_buf4,HEX);
      Serial.print(" ");
      Serial.print(can_buf5,HEX);
      Serial.println(" ");
      //can_send_command_func(0x64,0x05,0xCC,can_buf2,can_buf3,can_buf4,can_buf5,0,0);
      delay(1000);
////////////////////////////////////////////////////

      can_send_command_func(0x55,0x05,0xCC,0,0,0,0,0,0);

      Serial.println("control sequence is done");
      delay(100);
    }


  } //check complete command
}
void setup() 
{
  // put your setup code here, to run once:
  pinMode(tp35_addr,INPUT);
  pinMode(spoof_en_addr,OUTPUT);
  pinMode(cs_a_b_addr,OUTPUT);
  pinMode(cs_a_b_addr,OUTPUT);
  
  SPI.begin();
  digitalWrite(cs_a_b_addr,HIGH);
  delay(100);
  digitalWrite(cs_b_b_addr,HIGH);
  delay(100);
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  
  Serial.begin(115200);
  while(!Serial);

  
  dac_init();
  Serial.println("initial-CAN Receiver");

  if(!CAN.begin(500E3))
  {
    Serial.println("Starting CAN failed!");
    while(1);
  }
}
byte data_cnt = 0;


byte sent1_data1 = 0x1;
byte sent1_data2 = 0xe;
byte sent1_data3 = 0x5;
byte sent1_data4 = 0x8;
byte sent1_data5 = 0xc;
byte sent1_data6 = 0xb;

byte sent1_data1r = 0x1;
byte sent1_data2r = 0xe;
byte sent1_data3r = 0x5;
byte sent1_data4r = 0xb;
byte sent1_data5r = 0xc;
byte sent1_data6r = 0x8;

byte sent2_data1 = 0xe;
byte sent2_data2 = 0x1;
byte sent2_data3 = 0x5;
byte sent2_data4 = 0x2;
byte sent2_data5 = 0x3;
byte sent2_data6 = 0x4;

long senta1 = 450;
long senta2 = 3000;
long senta2r = (((senta2 ) & 0xF) << 8) | (((senta2 >> 4) & 0xF) << 4) | ((senta2 >> 8) & 0xF);
long sentb1 = 4096 - senta1;
long sentb2 = 4096 - senta2;
long sentb2r = (((sentb2 ) & 0xF) << 8) | (((sentb2 >> 4) & 0xF) << 4) | ((sentb2 >> 8) & 0xF);

void can_to_sent(long cha, long chb)
{
senta1 = cha;
senta2 = chb;
senta2r = (((senta2 ) & 0xF) << 8) | (((senta2 >> 4) & 0xF) << 4) | ((senta2 >> 8) & 0xF);
sentb1 = 0xfff - senta1;
sentb2 = 0xfff - senta2;
sentb2r = (((sentb2 ) & 0xF) << 8) | (((sentb2 >> 4) & 0xF) << 4) | ((sentb2 >> 8) & 0xF);

char can_sent0 = (0 << 4 ) | ((senta1 >> 8) & 0xF)  & 0xFF;
char can_sent1 =  (senta1 )                         & 0xFF;
char can_sent2 = (senta2r >> 4)                     & 0xFF ;
char can_sent3 = (((senta2r ) & 0xF) << 4 ) | 0x0   & 0xFF;
char can_sent4 = (0 << 4 ) | ((sentb1 >> 8) & 0xF)  & 0xFF;
char can_sent5 =  (sentb1 )                         & 0xFF;
char can_sent6 = (sentb2r >> 4)                     & 0xFF ;
char can_sent7 = (((sentb2r ) & 0xF) << 4 ) | 0x0   & 0xFF;
can_send_command_func(0xa1,can_sent0,can_sent1,can_sent2,can_sent3,can_sent4,can_sent5,can_sent6,can_sent7);
Serial.print(senta1);
Serial.print(" ");
Serial.println(senta2);
//delay(1000);
}

int sent_loop_cnt = 0;
void can_to_sent_loop(long cnt)
{
  if(sent_loop_cnt < cnt)
  {
      can_to_sent(131 - sent_loop_cnt , 2298 - sent_loop_cnt);
      
    sent_loop_cnt++;
  }
  else
  {

    sent_loop_cnt = 0;
  }
delay(100);
}
void loop() 
{
  // put your main code here, to run repeatedly:
  //
  Serial_command_func();
  //can_reiceve_read_func();                                              
  //can_reiceve_read_basic_func();
  //can_reiceve_wheel_func();
  //delay(10);
  //can_reiceve_func();

 //can_to_sent_loop(100);

can_to_sent(2770  , 3490 );
delay(1000);
//can_to_sent(28  , 2101 );
//delay(1000);
can_to_sent(2832  , 3610 );
delay(1000);
can_to_sent(3032  , 4010 );
delay(1000);


}
