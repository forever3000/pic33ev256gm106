#include "mcc_generated_files/system.h"
#include "mcc_generated_files/tmr1.h"
#include "mcc_generated_files/pin_manager.h"
#include "can.h"

extern int can_tx_flag;
extern int i_10us_flag;
extern int i_10ms_flag;
extern int i_100ms_flag;
extern int i_pwm1_flag;
int flag1,flag2,flag3,flag4;
char out1,out2,out3;

enum {
    PAS_CONTROL_ID = 0x129,
    ISG_CONTROL_ID = 0x124,
};

int main(void)
{
    SYSTEM_Initialize();
    InitCAN();  
    PAS_BUTTON_SetDigitalOutput();
    START_LED_SetDigitalOutput();
    LED1B_SetDigitalOutput();
    LED2R_SetDigitalOutput();
    LED2G_SetDigitalOutput();
    LED2B_SetDigitalOutput();
    LED3R_SetDigitalOutput();
    LED3G_SetDigitalOutput();
    LED3B_SetDigitalOutput();
    WINKER_SetDigitalOutput();
    ABS_CLK_SetDigitalOutput();
    
    int DATA,SNOW;
    int PAS_DATA=1;
    while (1)
    {
        if(KEY_GetValue()==0)
        {
            PAS_DATA=0;
        }
        if(i_100ms_flag)
        {
            i_100ms_flag=0;
            if(EM_GetValue())
            { 
                DATA=1;
            }
            if(START_GetValue())
            {
                DATA=3;
            }
            if(EM_GetValue()==1 && START_GetValue()==1)
            {
                DATA=2;
            }
            if(START_GetValue())
            {
                DATA=3;
            }
            CAN_ID = 0xCA;
            CAN_TX_DATA[0]=DATA;
            CAN_TX_DATA[1]=PAS_DATA;
            CAN_TX_DATA[2]=SNOW;
            CAN_Transmit();
            PAS_DATA=1;
        }
        if(can_rx_flag) // 0x129
        {
            can_rx_flag=0;
            if(canRxMessage.buffer_status == CAN_BUF_FULL)
            {
                rxECAN(&canRxMessage);
                canRxMessage.buffer_status = CAN_BUF_EMPTY;
                CAN_RData();
                
                if (CAN_RX_ID == PAS_CONTROL_ID) 
                {
                    if(CAN_RX_DATA[0])
                    {
                        PAS_BUTTON_SetHigh();
                    }
                    else 
                    {
                        PAS_BUTTON_SetLow();
                    }
                    
                    if(CAN_RX_DATA[1])
                    {
                        START_LED_SetHigh();
                    }
                    else 
                    {
                        START_LED_SetLow();
                    }
                }
                
                if(CAN_RX_ID == ISG_CONTROL_ID)
                {                    
                    if (CAN_RX_DATA[2] == 0x01)
                    {
                        LED1B_SetHigh();
                    }
                    else
                    {
                        LED1B_SetLow();
                    }
                }
            }
            else
            {
                UART1_Write("can_error");
            }
            UART1_Write(13);
        }
    }
    return 1; 
}
