/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.167.0
        Device            :  dsPIC33EV256GM106
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.50
        MPLAB 	          :  MPLAB X v5.35
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "mcc_generated_files/system.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/can1.h"
#include "mcc_generated_files/can_types.h"
#include "string.h"
#include "gear_control.h"

typedef uint8_t BOOL;
#define FALSE (0)
#define TRUE (1)

typedef struct {
    uint16_t s_tick;
    uint32_t CAN_ID;
    uint8_t CAN_TX_DATA[8];
} app_prm_t;

enum {
    GEAR_POS_CAN_ID = 0x111
};

void T1Interrupt(void);
void CAN1_RxBuffer(void);
void Delayus(int delay);

volatile app_prm_t g_app_info = {
    0,
    0,
    {0}
};

gear_pos_t gear_pos = UN_DEFINED;
gear_command_t gear_cmd = NONE_CMD;

/*
                         Main application
 */
int main(void)
{
    CAN_MSG_OBJ can_recv_msg;
    CAN_MSG_OBJ can_send_msg;
    uint8_t send_msg_data[8] = {0x00};
    uint8_t recv_msg_data[8] = {0x00};
    uint16_t gear_p_cnt = 0;
    uint16_t gear_d_cnt = 0;
    uint32_t moving_cnt = 0;
    const uint16_t max_cnt = 300; // 3s
    
    // Initialize buffer for can message
    can_send_msg.data = send_msg_data;
    can_recv_msg.data = recv_msg_data;
    
    // Initialize the device
    SYSTEM_Initialize();
    
    // Enable CAN Transmit
    CAN1_TransmitEnable();
    
    // Enable CAN Receive
    CAN1_ReceiveEnable();
    
    // Set the CAN interrupt callback
    CAN1_SetRxBufferInterruptHandler(&CAN1_RxBuffer);
    
    // Set the can configuration mode
    CAN1_OperationModeSet(CAN_CONFIGURATION_MODE);
    
    // Set the TIMER1 interrupt callback
    TMR1_SetInterruptHandler(&T1Interrupt);
    
    // Start the TIMER1
    TMR1_Start();

    // Wait CAN is ready
    while(CAN_CONFIGURATION_MODE != CAN1_OperationModeGet());
    while(CAN_OP_MODE_REQUEST_SUCCESS != CAN1_OperationModeSet(CAN_NORMAL_2_0_MODE));
    
    while (1)
    {
        // Get the current position of gear
        if(CAN1_ReceivedMessageCountGet() > 0) 
        {
            if (CAN1_Receive(&can_recv_msg))
            {
                if (can_recv_msg.msgId == GEAR_POS_CAN_ID)
                {
                    switch (can_recv_msg.data[1] & 0x0F) 
                    {
                        case 0x00:
                            gear_pos = PARKING_POS;
                            if (gear_p_cnt == 0 && gear_cmd == PARKING_CMD)
                            {
                                can_send_msg.msgId = 0x718;
                                can_send_msg.field.idType = 0;
                                can_send_msg.field.frameType = 0;
                                can_send_msg.field.dlc = 8;
                                memset(can_send_msg.data, 0x00, 8);
                                can_send_msg.data[0] = moving_cnt & 0xFF;
                                can_send_msg.data[1] = (moving_cnt & 0xFF00) >> 8;
                                can_send_msg.data[2] = (moving_cnt & 0xFF0000) >> 16;
                                can_send_msg.data[3] = (moving_cnt & 0xFF000000) >> 24;
                                CAN1_Transmit(CAN_PRIORITY_HIGH, &can_send_msg);
                                moving_cnt ++;
                            }
                            gear_p_cnt ++;
                            gear_d_cnt = 0;
                            break;
                        case 0x05:
                            gear_pos = DRIVING_POS;
                            gear_d_cnt ++;
                            gear_p_cnt = 0;
                            break;
                        case 0x06:
                            gear_pos = NEUTRAL_POS;
                            break;
                        case 0x07:
                            gear_pos = REVERSE_POS;
                            break;
                    }
                }
            }
        }
        
        // Control the gear every 100ms
        if (g_app_info.s_tick >= 10)
        {
            SendGearPos(gear_pos, gear_cmd);
            g_app_info.s_tick = 0;
        }
        
        if (gear_p_cnt > max_cnt)
        {
            gear_p_cnt = 0;
            gear_cmd = DRIVING_CMD;
        }
        
        if (gear_d_cnt > max_cnt)
        {
            gear_d_cnt = 0;
            gear_cmd = PARKING_CMD;
        }
        
        Delayus(100);
    }
    
    return 1; 
}

void T1Interrupt(void)
{
    g_app_info.s_tick ++;
}

void CAN1_RxBuffer(void)
{
    // CAN1 Receive buffer application code
}

void Delayus(int delay)
{
    int i;
    for (i = 0; i < delay; i++)
    {
        __asm__ volatile ("repeat #39");
        __asm__ volatile ("nop");
    }
}

/**
 End of File
*/

