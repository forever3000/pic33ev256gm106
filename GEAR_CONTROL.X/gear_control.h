/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#ifndef _GEAR_CONTROL_H
#define _GEAR_CONTROL_H

#include "stdint.h"
#include "string.h"
#include "mcc_generated_files/uart2.h"

typedef enum {
    UN_DEFINED = 0,
    PARKING_POS = 1,
    REVERSE_POS = 2,
    NEUTRAL_POS = 3,
    DRIVING_POS = 4
} gear_pos_t;

typedef enum {
    NONE_CMD = 0,
    PARKING_CMD = 1,
    REVERSE_CMD = 2,
    NEUTRAL_CMD = 3,
    DRIVING_CMD = 4,
} gear_command_t;

typedef enum {
    NORMAL = 0,
    ABNORMAL = 1
} operation_state_t;

void SendGearPos(gear_pos_t curr_pos, gear_command_t cmd);

#endif  //_GEAR_CONTROL_H
/**
 End of File
*/