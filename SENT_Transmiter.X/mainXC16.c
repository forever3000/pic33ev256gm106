/*
 * @file   mainXC16.c
 * @author toanvo
 * @notice Copyright (C) ADASONE Inc. All rights reserved.
 */

 /*
  * Software designed to be loaded to the dsPIC33EV256GM106 Starter Kit
  *
  * What the code does:
  *    This program is used to study SENT data from Torque Angle Sensor (TAS) of Kia Sportage
  * a) SENT1 and SENT2 are all available as receiver mode
  * b) Received data from SENT1 and SENT2 are packed to CAN message and transmitted out for logging purpose
  * c) LED1 actives when SENT1 receives data
  * d) LED2 actives when SENT2 receives data
  *
  *                                      +--------+
  *                                      |        |
  *                                      | Torque |
  *                                      | sensor |
  *                                      +--------+
  * +-----+        +-----------+                
  * |     |        |           |SENT1--------+   
  * | PC  |<--->CAN| dsPIC33EV |             |   
  * |     |        |           |SENT2--------|---+
  * +-----+        +-----------+             V   V
  *                                      +--------+
  *                                      |        |
  *                                      |  ECU   |
  *                                      |        |
  *                                      +--------+
  * 
  * 
  * Be SURE the ADM00393 has the power select jumper set to 5V! There are 3 jumper wires needed.
  *
  * TXD - RB4 goes to the RXD pin of the ADM
  * GND - GND goes to the G pin of the ADM
  * DVDD - DVDD goes to the VDD pin of the ADM
  *
  * The demo UART baud rate is set to 38,400/8/N/1.
  *
  *************************************************************************************************/

#include "xc.h"

#include <stdint.h>

//
// includes for the header files
//
// port definitions for the starter kit
//
//#define         LED1        _LATC4
//#define         TRISLED1    _TRISC4
//#define         LED2        _LATC5
//#define         TRISLED2    _TRISC5
//#define         LED3        _LATC6
//#define         TRISLED3    _TRISC6
//#define         SW1         _RC7
//#define         SW2         _RC8
//#define         SW3         _RC9
#define         RELAY       _LATA12
#define         TRISRELAY   _TRISA12
#define         RELAY_STS   _RA12

#define         FCAN        40000000                // Fcyc = 1/2Fpll
#define         BAUD9600    ((FCAN/9600)/16) - 1
#define         BAUD19200   ((FCAN/19200)/16) - 1
#define         BAUD38400   ((FCAN/38400)/16) - 1   // this is what the demo UART serial baud rate is
#define         BAUD576000  ((FCAN/57600)/16) - 1   // selection of transmitter baud rate divisors
#define         ANSEL_RTS   _ANSE12
#define         ANSEL_CTS   _ANSE13
#define         TRIS_RTS    _TRISE12
#define         TRIS_MON    _TRISB4

/* CAN filter and mask defines */
/* Macro used to write filter/mask ID to Register CiRXMxSID and
CiRXFxSID. For example to setup the filter to accept a value of
0x123, the macro when called as CAN_FILTERMASK2REG_SID(0x123) will
write the register space to accept message with ID 0x123
USE FOR STANDARD MESSAGES ONLY */
#define CAN_FILTERMASK2REG_SID(x) ((x & 0x07FF)<< 5)
/* the Macro will set the "MIDE" bit in CiRXMxSID */
#define CAN_SETMIDE(sid) (sid | 0x0008)
/* the macro will set the EXIDE bit in the CiRXFxSID to
accept extended messages only */
#define CAN_FILTERXTD(sid) (sid | 0x0008)
/* the macro will clear the EXIDE bit in the CiRXFxSID to
accept standard messages only */
#define CAN_FILTERSTD(sid) (sid & 0xFFF7)

//  Macros for Configuration Fuse Registers 
#if 1
_FOSCSEL(FNOSC_PRIPLL);
_FOSC(FCKSM_CSDCMD & OSCIOFNC_OFF & POSCMD_XT);
// Startup directly into XT + PLL
// OSC2 Pin Function: OSC2 is Clock Output
// Primary Oscillator Mode: XT Crystal

_FWDT(FWDTEN_OFF);      // Watchdog Timer Enabled/disabled by user software

_FICD(ICS_PGD2);        // PGD3 for external PK3/ICD3/RealIce, use PGD2 for PKOB
_FPOR(BOREN0_OFF);      // no brownout detect
_FDMT(DMTEN_DISABLE);   // no deadman timer  <<< *** New feature, important to DISABLE

#else
#pragma config FNOSC = FRCPLL
#endif

#define NUM_OF_ECAN_BUFFERS 32
#define MSG_SID 0xD0              // the arbitrary CAN SID of the transmitted message

/* ECAN message type identifiers */
#define CAN_MSG_DATA 0x01
#define CAN_MSG_RTR 0x02
#define CAN_FRAME_EXT 0x03
#define CAN_FRAME_STD 0x04
#define CAN_BUF_FULL 0x05
#define CAN_BUF_EMPTY 0x06

#define NUM_DIGITS 5               // floating point digits to print
#define STRING_BUFFER_SIZE 64      // arbitrary length message buffer

volatile unsigned int ecan1MsgBuf[NUM_OF_ECAN_BUFFERS][8]
__attribute__((aligned(NUM_OF_ECAN_BUFFERS * 16)));

typedef uint8_t BOOL;
#define FALSE (0)
#define TRUE (1)
#define TRANSMITTER 1
#define SELF_TEST 0
#define EXT_CONTROL_TIMEOUT 300

/* CAN receive message structure in RAM */
typedef struct{
	/* keep track of the buffer status */
	unsigned char buffer_status;
	/* RTR message or data message */
	unsigned char message_type;
	/* frame type extended or standard */
	unsigned char frame_type;
	/* buffer being used to send and receive messages */
	unsigned char buffer;
	/* 29 bit id max of 0x1FFF FFFF
	*  11 bit id max of 0x7FF */
	unsigned long id;
	unsigned int data[8];
	unsigned char data_length;
}mID;

// Prototype Declarations
void rxECAN(mID *message);
void clearRxFlags(unsigned char buffer_number);
void InitSENT1_TX(void);
void InitSENT2_TX(void);
void oscConfig(void);
void clearIntrflags(void);
void ecan1WriteMessage(void);
void init_hw(void);
void delay_10ms(unsigned char num);
void Delayus(int);
void InitMonitor(void);
void InitCAN(void);
void CAN_Transmit(void);
void ftoa(float, char*);
void SENT_transmit(void);
// send a character to the serial port
void putU2(int);
void putsU2(char*);
inline void PackSent1RxToCanMsg();
inline void PackSent2RxToCanMsg();

typedef struct {
    BOOL is_rx;
    uint8_t cnt;
    uint16_t datal;
    uint16_t datah;
} SENTPrm_t;

typedef struct {
    int f_tick;
    int s_tick;
    const float tickTime;
    const float peripheralClk;
    char can_rx;
    SENTPrm_t sent1_prm;
    SENTPrm_t sent2_prm;
    uint8_t CAN_ID;
    uint8_t CAN_TX_DATA[8];
    uint32_t counter;
} AppPrm_t;

volatile AppPrm_t g_app_info = {
    0,
    0,
    1.7,
    39,77,
    0,
    {0},
    {0},
    0,
    {0}
};

mID canRxMessage;

#define SENT_STS (0x8)
enum {
    CAN_ID_PC = 0xD0,
    CAN_ID_RECEIVER = 0xDD,
    OSCC_STEERING_ENABLE_CAN_ID = 0x54,
    OSCC_STEERING_DISABLE_CAN_ID = 0x55,
};

/* 0x123x -> 0x0321 */
static inline uint16_t swapNibbles(uint16_t val)
{
    uint16_t ret = ((val >> 12) & 0x000F) | 
                   ((val >>  4) & 0x00F0) |
                   ((val <<  4) & 0x0F00) | 
                   ((val << 12) & 0xF000);
    return ret;
}

int main(void)
{
    uint16_t torque_test_1[2] = {0x0000, 0x0000};
    uint16_t torque_test_2[2] = {0x0000, 0x0000};
    uint16_t torque_val_1[2] = {0x0000, 0x0000};
    uint16_t torque_val_2[2] = {0x0000, 0x0000};
    uint16_t torque_offset[2] = {0x0000, 0x0000};
    uint32_t status_1;
    uint32_t status_2;
    uint8_t offset_status = 0;
    BOOL is_controlling = FALSE;
    BOOL sent_init = FALSE;
    
    // Init counter
    g_app_info.counter = 0;
    
    // Configure Oscillator Clock Source
    oscConfig();

    // Clear Interrupt Flags
    clearIntrflags();

    // Initialize hardware on the board
    init_hw();

    // Initialize the monitor UART2 module
    InitMonitor();

    // Initialize the modules for receive or transmit
    InitSENT1_TX();
    InitSENT2_TX();

    // Initialize the CAN module
    InitCAN();

    // main loop
    g_app_info.s_tick = 0;

    while (1) {
        //if(canRxMessage.buffer_status == CAN_BUF_FULL) 
        {
            rxECAN(&canRxMessage);

            if(canRxMessage.id == CAN_ID_RECEIVER) {
                g_app_info.sent1_prm.datah = (canRxMessage.data[0] << 8) | canRxMessage.data[1];
                g_app_info.sent1_prm.datal = (canRxMessage.data[2] << 8) | canRxMessage.data[3];
                g_app_info.sent2_prm.datah = (canRxMessage.data[4] << 8) | canRxMessage.data[5];
                g_app_info.sent2_prm.datal = (canRxMessage.data[6] << 8) | canRxMessage.data[7];
                
                //test
                //is_controlling = TRUE;
            } else if(canRxMessage.id == CAN_ID_PC) {
                // Reset the counter
                g_app_info.counter = 0;
                offset_status = (canRxMessage.data[0] & 0xF0) >> 4;
                torque_offset[0] = ((canRxMessage.data[0] << 8) | canRxMessage.data[1]) & 0x0FFF;
                torque_offset[1] = swapNibbles((canRxMessage.data[2] << 8) | canRxMessage.data[3]) & 0x0FFF;
            }
            else if (canRxMessage.id == OSCC_STEERING_ENABLE_CAN_ID)
            {
                is_controlling = TRUE;
                RELAY = 1;
            }
            else if (canRxMessage.id == OSCC_STEERING_DISABLE_CAN_ID)
            {
                is_controlling = FALSE;
                RELAY = 0;
            }
            
            //test
            if(is_controlling == TRUE && sent_init == FALSE) {
                // Turn on the relay
                RELAY = 1;
                
                // Get status, torque from SENT1 message
                status_1 = (g_app_info.sent1_prm.datah & 0xF000); 
                torque_val_1[0] = g_app_info.sent1_prm.datah & 0x0FFF;
                torque_val_1[1] = swapNibbles(g_app_info.sent1_prm.datal) & 0x0FFF;
                torque_test_1[0] = (torque_val_1[0]) & 0x0FFF;
                torque_test_1[1] = (torque_val_1[1]) & 0x0FFF;
                
                // Get status, torque from SENT2 message
                status_2 = (g_app_info.sent2_prm.datah & 0xF000);
                torque_val_2[0] = g_app_info.sent2_prm.datah & 0x0FFF;
                torque_val_2[1] = swapNibbles(g_app_info.sent2_prm.datal) & 0x0FFF;;
                torque_test_2[0] = (torque_val_2[0]) & 0x0FFF;
                torque_test_2[1] = (torque_val_2[1]) & 0x0FFF;
                sent_init = TRUE;
            }
            else if (is_controlling == TRUE && sent_init == TRUE)
            {
                if (g_app_info.counter >= EXT_CONTROL_TIMEOUT)
                {
                    torque_offset[0] = 0;
                    torque_offset[1] = 0;
                }
                // Get status, torque from SENT1 message
                status_1 = (g_app_info.sent1_prm.datah & 0xF000);
                
                // Get status, torque from SENT1 message
                torque_val_1[0] = g_app_info.sent1_prm.datah & 0x0FFF;
                torque_val_1[1] = swapNibbles(g_app_info.sent1_prm.datal) & 0x0FFF;
                if (offset_status == 0)
                {
                    torque_test_1[0] = (torque_val_1[0] + torque_offset[0]) & 0x0FFF;
                    torque_test_1[1] = (torque_val_1[1] + torque_offset[1]) & 0x0FFF;               
                }
                else if (offset_status == 1)
                {
                    torque_test_1[0] = (torque_val_1[0] - torque_offset[0]) & 0x0FFF;
                    torque_test_1[1] = (torque_val_1[1] - torque_offset[1]) & 0x0FFF;                   
                }
            }

            canRxMessage.buffer_status = CAN_BUF_EMPTY;
        }
        
#if SELF_TEST
        is_controlling = TRUE;
        RELAY = 1;
        g_app_info.sent1_prm.datah = 0x11;
        g_app_info.sent1_prm.datal = 0x22;
        g_app_info.sent2_prm.datah = 0xFF;
        g_app_info.sent2_prm.datal = 0xFF;
#endif
        
        if(FALSE == is_controlling) {
            //Don't care, the Relay does the job
        } else {
#if 0
            // Assign sending data to SENT registers
            SENT1DATH = g_app_info.sent1_prm.datah;
            SENT1DATL = g_app_info.sent1_prm.datal;
            SENT2DATH = g_app_info.sent2_prm.datah;
            SENT2DATL = g_app_info.sent2_prm.datal;
#else            
            SENT1DATH = status_1 | (torque_test_1[0] & 0x0FFF);
            SENT1DATL = swapNibbles(torque_test_1[1]) & 0xFFF0;
            SENT2DATH = status_2 | (~torque_test_1[0] & 0x0FFF);
            SENT2DATL = swapNibbles((~torque_test_1[1]) & 0x0FFF) & 0xFFF0; 
            //SENT2DATH = status_2 | (torque_test_2[0] & 0x0FFF);
            //SENT2DATL = swapNibbles(torque_test_2[1]) & 0xFFF0;
#endif      
            //Send out CAN message in order to debug
            PackSent1RxToCanMsg();
            CAN_Transmit();
            
            // Turn on status bit in order to start sending
            SENT1STATbits.SYNCTXEN = 1;
            SENT2STATbits.SYNCTXEN = 1;
            
            Delayus(20);
            
            //Reset SENT status bit
            SENT1STATbits.SYNCTXEN = 0;
            SENT2STATbits.SYNCTXEN = 0;
        }
    }
}

void InitSENT1_TX(void)
{
    //
    // initialize the SENT hardware port on the Starter Kit
    //
    RPOR8bits.RP69R = 0x39;     // map SENT1 transmitter to pin RD5, low byte
    _TRISD5 = 0;                // digital output pin

    // Set up SENT interrupts
    IPC45bits.SENT1IP = 5;      // SENT TX/RX completion interrupt priority
    IFS11bits.SENT1IF = 0;      // Clear SENT TX/RX completion interrupt flag
    IEC11bits.SENT1IE = 1;      // Enable SENT TX/RX completion interrupt

    IPC45bits.SENT1EIP = 6;     // SENT ERROR interrupt priority
    IFS11bits.SENT1EIF = 0;     // Clear SENT ERROR interrupt flag
    IEC11bits.SENT1EIE = 1;     // Enable SENT ERROR interrupt

    // Initialize SENT registers for transmit mode (no frame time specified due to no pause)
    SENT1CON2 = (int)(g_app_info.tickTime * g_app_info.peripheralClk) - 1;
    SENT1CON1bits.RCVEN = 0;    //Transmit mode
    SENT1CON1bits.TXM = 1;      // sync handshaking mode
    SENT1CON1bits.CRCEN = 1;    // CRC enable, 0=off, 1=on
    SENT1CON1bits.PPP = 0;      // Pause, 0=off, 1=on
    SENT1CON1bits.NIBCNT = 6;   // nibbles of data
    SENT1CON1bits.SNTEN = 1;    // enable SENT module
    SENT1DATH = 0;
    SENT1DATL = 0;              // initialize the SENT data registers
}

void InitSENT2_TX(void)
{
    //
    // initialize the SENT hardware port on the Starter Kit
    //
    RPOR8bits.RP70R = 0x3A;     // map SENT1 transmitter to pin RD6, low byte
    _TRISD6 = 0;                // digital output pin

    // Set up SENT interrupts
    IPC46bits.SENT2IP = 5;      // SENT TX/RX completion interrupt priority
    IFS11bits.SENT2IF = 0;      // Clear SENT TX/RX completion interrupt flag
    IEC11bits.SENT2IE = 1;      // Enable SENT TX/RX completion interrupt

    IPC46bits.SENT2EIP = 6;     // SENT ERROR interrupt priority
    IFS11bits.SENT2EIF = 0;     // Clear SENT ERROR interrupt flag
    IEC11bits.SENT2EIE = 1;     // Enable SENT ERROR interrupt

    // Initialize SENT registers for transmit mode (no frame time specified due to no pause)
    SENT2CON2 = (int)(g_app_info.tickTime * g_app_info.peripheralClk) - 1;
    SENT2CON1bits.RCVEN = 0;    //Transmit mode
    SENT2CON1bits.TXM = 1;      // sync handshaking mode
    SENT2CON1bits.CRCEN = 1;    // CRC enable, 0=off, 1=on
    SENT2CON1bits.PPP = 0;      // Pause, 0=off, 1=on
    SENT2CON1bits.NIBCNT = 6;   // nibbles of data
    SENT2CON1bits.SNTEN = 1;    // enable SENT module
    SENT2DATH = 0;
    SENT2DATL = 0;              // initialize the SENT data registers
}

void clearIntrflags(void)
{
    /* Clear Interrupt Flags */

    IFS0 = 0;
    IFS1 = 0;
    IFS2 = 0;
    IFS3 = 0;
    IFS4 = 0;
    IPC16bits.U1EIP = 6;        //service the LIN framing error before the RX
    IPC2bits.U1RXIP = 4;
}

void init_hw(void)
{
    TRISRELAY = 0;
    RELAY = 0;

    // set up the LED and switch ports
    ANSELC = ANSELC & 0xFC3F;   // (re)set the 3 switch bits + CAN due to error in v1.20 header
    g_app_info.s_tick = 0;
    g_app_info.f_tick = 0;                 // the timer ticks

    //
    // Timer 1 to generate an interrupt every 10ms
    //
    T1CONbits.TON = 0;          // Disable Timer1
    T1CONbits.TCS = 0;          // Select internal instruction cycle clock
    T1CONbits.TGATE = 0;        // Disable Gated Timer mode
    T1CONbits.TCKPS = 0x3;      // Select 1:256 Prescaler
    PR1 = 1562;                 // Load the period value (10ms/(256*25ns))
    IPC0bits.T1IP = 0x03;       // Set Timer 1 Interrupt Priority Level
    IFS0bits.T1IF = 0;          // Clear Timer 1 Interrupt Flag
    IEC0bits.T1IE = 1;          // Enable Timer1 interrupt

#if 1
    //
    // Timer 2 to generate an interrupt every 10ms
    //
    T2CONbits.TON = 0;          // Disable Timer2
    T2CONbits.TCS = 0;          // Select internal instruction cycle clock
    T2CONbits.TGATE = 0;        // Disable Gated Timer mode
    T2CONbits.TCKPS = 0x3;      // Select 1:256 Prescaler
    TMR2 = 0x00;                // Clear timer register
    PR2 = 42;                 // Load the period value (10ms/(256*25ns))
    IPC1bits.T2IP = 0x02;       // Set Timer 2 Interrupt Priority Level
    IFS0bits.T2IF = 0;          // Clear Timer 2 Interrupt Flag
    IEC0bits.T2IE = 1;          // Enable Timer2 interrupt

    T2CONbits.TON = 1;          // Start Timer2
#endif
    T1CONbits.TON = 1;          // Start Timer1
}

void oscConfig(void)
{

    //  Configure Oscillator to operate the device at 80MHz/40MIPs
    // 	Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
    // 	Fosc= 8M*40/(2*2)=80Mhz for 8M input clock
    // To be safe, always load divisors before feedback
    
   
    CLKDIVbits.PLLPOST = 0;     // N1=2
    CLKDIVbits.PLLPRE = 0;      // N2=2
    PLLFBD = 38;                // M=(40-2), Fcyc = 40MHz for ECAN baud timer


    // Disable Watch Dog Timer

    RCONbits.SWDTEN = 0;

}

void InitCAN(void)
{
    //
    // drive the CAN STANDBY driver pin low
    //
    _TRISG9 = 0;
    _LATG9 = 0;
    _TRISF1 = 0;
    _TRISF0 = 1;

    //
    // remap the CAN module to the proper pins on the board
    //
    RPINR26 = 0x60;         // connect CAN RX to RPI96
    RPOR9 = 0x000E;         // connect CAN TX to RP97

    C1CTRL1bits.REQOP = 4;

    while (C1CTRL1bits.OPMODE != 4);
    C1CTRL1bits.WIN = 0;

    /* Set up the CAN module for 250kbps speed with 10 Tq per bit. */

    C1CFG1 = 0x83;          // BRP = 8 SJW = 2 Tq
    C1CFG2 = 0x2D2;
    C1FCTRL = 0xC01F;       // No FIFO, 32 Buffers

    //
    // set up the CAN DMA0 for the Transmit Buffer
    //
    DMA0CONbits.SIZE = 0x0;
    DMA0CONbits.DIR = 0x1;
    DMA0CONbits.AMODE = 0x2;
    DMA0CONbits.MODE = 0x0;
    DMA0REQ = 70;
    DMA0CNT = 7;
    DMA0PAD = (volatile unsigned int)&C1TXD;
    DMA0STAL = (unsigned int)&ecan1MsgBuf;
    DMA0STAH = (unsigned int)&ecan1MsgBuf;

    C1TR01CONbits.TXEN0 = 0x1;          // Buffer 0 is the Transmit Buffer
    C1TR01CONbits.TX0PRI = 0x3;         // transmit buffer priority

    DMA0CONbits.CHEN = 0x1;

    /* initialise the DMA channel 2 for ECAN Rx */
;
    /* setup channel 2 for peripheral indirect addressing mode
    normal operation, word operation and select as Rx to peripheral */
    DMA2CON = 0x0020;
    /* setup the address of the peripheral ECAN1 (C1RXD) */
	DMA2PAD = (volatile unsigned int)&C1RXD;
 	/* Set the data block transfer size of 8 */
 	DMA2CNT = 7;
 	/* automatic DMA Rx initiation by DMA request */
	DMA2REQ = 0x0022;
	/* start adddress offset value */
	DMA2STAL=(unsigned int)(&ecan1MsgBuf);
    DMA2STAH=(unsigned int)(&ecan1MsgBuf);
	/* enable the channel */
	DMA2CONbits.CHEN=1;

	/* 4 CAN Messages to be buffered in DMA RAM */
	C1FCTRLbits.DMABS=0b000;

    /* Filter configuration */
	/* enable window to access the filter configuration registers */
	C1CTRL1bits.WIN = 0b1;
	/* select acceptance mask 0 filter 0 buffer 1 */
	C1FMSKSEL1bits.F0MSK = 0;

    /* setup the mask to check every bit of the standard message, the macro when called as */
    /* CAN_FILTERMASK2REG_SID(0x7FF) will write the register C1RXM0SID to include every bit in */
    /* filter comparison */
    C1RXM0SID=CAN_FILTERMASK2REG_SID(0x7F0);
    C1RXM1SID=CAN_FILTERMASK2REG_SID(0x7FF);
    C1RXM2SID=CAN_FILTERMASK2REG_SID(0x7FF);

	/* configure accpetence filter 0
	setup the filter to accept a standard id of 0x123,
	the macro when called as CAN_FILTERMASK2REG_SID(0x123) will
	write the register C1RXF0SID to accept only standard id of 0x123
	*/
	C1RXF0SID = CAN_FILTERMASK2REG_SID(MSG_SID);
    C1RXF1SID = CAN_FILTERMASK2REG_SID(CAN_ID_RECEIVER);
	C1RXF2SID = CAN_FILTERMASK2REG_SID(OSCC_STEERING_ENABLE_CAN_ID);
    C1RXF2SID = CAN_FILTERMASK2REG_SID(OSCC_STEERING_DISABLE_CAN_ID);
	/* set filter to check for standard ID and accept standard id only */
	C1RXM0SID = CAN_SETMIDE(C1RXM0SID);
	C1RXF0SID = CAN_FILTERSTD(C1RXF0SID);
	/* acceptance filter to use buffer 1 for incoming messages */
	C1BUFPNT1bits.F0BP = 0b0001;
    C1BUFPNT1bits.F1BP = 0b0001;
    C1BUFPNT1bits.F2BP = 0b0001;
    C1BUFPNT1bits.F3BP = 0b0001;    
	/* enable filter 0 */
	C1FEN1bits.FLTEN0 = 1;
    C1FEN1bits.FLTEN1 = 1;
    C1FEN1bits.FLTEN2 = 1;
    C1FEN1bits.FLTEN3 = 1;
    /* clear window bit to access ECAN control registers */
	C1CTRL1bits.WIN = 0;

    /* ECAN1, Buffer 1 is a Receive Buffer */
	C1TR01CONbits.TXEN1 = 0;

    /* clear the buffer and overflow flags */
	C1RXFUL1=C1RXFUL2=C1RXOVF1=C1RXOVF2=0x0000;

    // Place the ECAN module in Normal mode.
    C1CTRL1bits.REQOP = 0;
    while (C1CTRL1bits.OPMODE != 0);

    //
    // CAN RX interrupt enable - 'double arm' since 2-level nested interrupt
    //
    C1INTEbits.RBIE = 1;
    IEC2bits.C1IE = 1;
}

void PackSent1RxToCanMsg()
{
    IEC11bits.SENT1IE = 0;
    g_app_info.CAN_TX_DATA[0] = (g_app_info.sent1_prm.datah >> 8) & 0xFF;
    g_app_info.CAN_TX_DATA[1] = (g_app_info.sent1_prm.datah >> 0) & 0xFF;
    g_app_info.CAN_TX_DATA[2] = (g_app_info.sent1_prm.datal >> 8) & 0xFF;
    g_app_info.CAN_TX_DATA[3] = (g_app_info.sent1_prm.datal >> 0) & 0xFF;
    IEC11bits.SENT1IE = 1;
}

void PackSent2RxToCanMsg()
{
    IEC11bits.SENT2IE = 0;
    g_app_info.CAN_TX_DATA[4] = (g_app_info.sent2_prm.datah >> 8) & 0xFF;
    g_app_info.CAN_TX_DATA[5] = (g_app_info.sent2_prm.datah >> 0) & 0xFF;
    g_app_info.CAN_TX_DATA[6] = (g_app_info.sent2_prm.datal >> 8) & 0xFF;
    g_app_info.CAN_TX_DATA[7] = (g_app_info.sent2_prm.datal >> 0) & 0xFF;
    IEC11bits.SENT2IE = 1;
}

void CAN_Transmit(void)
{
    g_app_info.CAN_ID = 0xaa;

    ecan1MsgBuf[0][0] = g_app_info.CAN_ID << 2;

    ecan1MsgBuf[0][1] = 0x0000;
    /* CiTRBnDLC = 0b0000 0000 xxx0 1111
    EID<17:6> = 0b000000
    RTR = 0b0
    RB1 = 0b0
    RB0 = 0b0
    DLC = 6 */
    //ecan1MsgBuf[0][2] = 0x0006;
    ecan1MsgBuf[0][2] = 0x0008;

    // Write message 6 data bytes as follows:
    //
    // POTH POTL TEMPH TEMPL 0000 S3S2S1
    //
    ecan1MsgBuf[0][3] = (g_app_info.CAN_TX_DATA[1] << 8) | (g_app_info.CAN_TX_DATA[0]);
    ecan1MsgBuf[0][4] = (g_app_info.CAN_TX_DATA[3] << 8) | (g_app_info.CAN_TX_DATA[2]);
    ecan1MsgBuf[0][5] = (g_app_info.CAN_TX_DATA[5] << 8) | (g_app_info.CAN_TX_DATA[4]);
    ecan1MsgBuf[0][6] = (g_app_info.CAN_TX_DATA[7] << 8) | (g_app_info.CAN_TX_DATA[6]);


    Nop();
    Nop();
    Nop();
    /* Request message buffer 0 transmission */
    C1TR01CONbits.TXREQ0 = 0x1;
    /* The following shows an example of how the TXREQ bit can be polled to check if transmission
    is complete. */
    Nop();
    Nop();
    Nop();
    while (C1TR01CONbits.TXREQ0 == 1);
    // Message was placed successfully on the bus, return
}

void InitMonitor(void)
{
    // digital output
    TRIS_MON = 0;

    //
    // map MONITOR_TX pin to port RB4, which is remappable RP36
    //
    RPOR1bits.RP36R = 0x03; // map UART2 TXD to pin RB4
    //
    // set up the UART for default baud, 1 start, 1 stop, no parity
    //
    U2MODEbits.STSEL = 0;       // 1-Stop bit
    U2MODEbits.PDSEL = 0;       // No Parity, 8-Data bits
    U2MODEbits.ABAUD = 0;       // Auto-Baud disabled
    U2MODEbits.BRGH = 0;        // Standard-Speed mode
    U2BRG = BAUD38400;          // Baud Rate setting for 38400 (default)
    U2STAbits.UTXISEL0 = 0;     // Interrupt after TX buffer done
    U2STAbits.UTXISEL1 = 1;
    IEC1bits.U2TXIE = 1;        // Enable UART TX interrupt
    U2MODEbits.UARTEN = 1;      // Enable UART (this bit must be set *BEFORE* UTXEN)

}

void delay_10ms(unsigned char num)
{
    g_app_info.f_tick = 0;                         //f_tick increments every 10ms
    while (g_app_info.f_tick < num);               // wait here until 'num' ticks occur
    g_app_info.f_tick = 0;
}

void Delayus(int delay)
{
    int i;
    for (i = 0; i < delay; i++)
    {
        __asm__ volatile ("repeat #39");
        __asm__ volatile ("nop");
    }
}

//*****************************************************************************
//
// Float to ASCII
//
// Converts a floating point number to ASCII. Note that buf must be
// large enough to hold result (in this case 4 digits)
//
// f is the floating point number.
// buf is the buffer in which the resulting string is placed.
//
// ftoa(1.23) returns "1.23"
//
//
//*****************************************************************************

void ftoa(float f, char *buf)
{
    int pos, ix, dp, num;
    pos = 0;
    ix = 0;
    dp = 0;
    num = 0;

    if (f < 0)
    {
        buf[pos++] = '-';
        f = -f;
    }
    dp = 0;
    while (f >= 10.0)
    {
        f = f / 10.0;
        dp++;
    }
    for (ix = 1; ix < (NUM_DIGITS + 1); ix++)
    {
        num = (int)f;
        f = f - num;
        buf[pos++] = '0' + num;
        if (dp == 0) buf[pos++] = '.';
        f = f * 10.0;
        dp--;
    }
}

void putsU2(char *s)
{
    while (*s)
    { // loop until *s =\0, end of string
        putU2(*s++);
    } // send the character and point to the next one
}

void putU2(int c)
{
    while (U2STAbits.UTXBF); // wait while Tx buffer full
    U2TXREG = c;
}

/******************************************************************************
*
*    Function:			rxECAN
*    Description:       moves the message from the DMA memory to RAM
*
*    Arguments:			*message: a pointer to the message structure in RAM
*						that will store the message.
******************************************************************************/
void rxECAN(mID *message)
{
	unsigned int ide=0;
	unsigned int rtr=0;
	unsigned long id=0;

	/*
	Standard Message Format:
	Word0 : 0bUUUx xxxx xxxx xxxx
			     |____________|||
 					SID10:0   SRR IDE(bit 0)
	Word1 : 0bUUUU xxxx xxxx xxxx
			   	   |____________|
						EID17:6
	Word2 : 0bxxxx xxx0 UUU0 xxxx
			  |_____||	     |__|
			  EID5:0 RTR   	  DLC
	word3-word6: data bytes
	word7: filter hit code bits

	Remote Transmission Request Bit for standard frames
	SRR->	"0"	 Normal Message
			"1"  Message will request remote transmission
	Substitute Remote Request Bit for extended frames
	SRR->	should always be set to "1" as per CAN specification

	Extended  Identifier Bit
	IDE-> 	"0"  Message will transmit standard identifier
	   		"1"  Message will transmit extended identifier

	Remote Transmission Request Bit for extended frames
	RTR-> 	"0"  Message transmitted is a normal message
			"1"  Message transmitted is a remote message
	Don't care for standard frames
	*/

	/* read word 0 to see the message type */
	ide=ecan1MsgBuf[message->buffer][0] & 0x0001;

	/* check to see what type of message it is */
	/* message is standard identifier */
	if(ide==0)
	{
		message->id=(ecan1MsgBuf[message->buffer][0] & 0x1FFC) >> 2;
		message->frame_type=CAN_FRAME_STD;
		rtr=ecan1MsgBuf[message->buffer][0] & 0x0002;
	}
	/* mesage is extended identifier */
	else
	{
		id=ecan1MsgBuf[message->buffer][0] & 0x1FFC;
		message->id=id << 16;
		id=ecan1MsgBuf[message->buffer][1] & 0x0FFF;
		message->id=message->id+(id << 6);
		id=(ecan1MsgBuf[message->buffer][2] & 0xFC00) >> 10;
		message->id=message->id+id;
		message->frame_type=CAN_FRAME_EXT;
		rtr=ecan1MsgBuf[message->buffer][2] & 0x0200;
	}
	/* check to see what type of message it is */
	/* RTR message */
	if(rtr==1)
	{
		message->message_type=CAN_MSG_RTR;
	}
	/* normal message */
	else
	{
		message->message_type=CAN_MSG_DATA;
		message->data[0]=(unsigned char)ecan1MsgBuf[message->buffer][3];
		message->data[1]=(unsigned char)((ecan1MsgBuf[message->buffer][3] & 0xFF00) >> 8);
		message->data[2]=(unsigned char)ecan1MsgBuf[message->buffer][4];
		message->data[3]=(unsigned char)((ecan1MsgBuf[message->buffer][4] & 0xFF00) >> 8);
		message->data[4]=(unsigned char)ecan1MsgBuf[message->buffer][5];
		message->data[5]=(unsigned char)((ecan1MsgBuf[message->buffer][5] & 0xFF00) >> 8);
		message->data[6]=(unsigned char)ecan1MsgBuf[message->buffer][6];
		message->data[7]=(unsigned char)((ecan1MsgBuf[message->buffer][6] & 0xFF00) >> 8);
		message->data_length=(unsigned char)(ecan1MsgBuf[message->buffer][2] & 0x000F);
	}
	clearRxFlags(message->buffer);
}

/******************************************************************************
*
*    Function:			clearRxFlags
*    Description:       clears the rxfull flag after the message is read
*
*    Arguments:			buffer number to clear
******************************************************************************/
void clearRxFlags(unsigned char buffer_number)
{
	if((C1RXFUL1bits.RXFUL1) && (buffer_number==1))
		/* clear flag */
		C1RXFUL1bits.RXFUL1=0;
	/* check to see if buffer 2 is full */
	else if((C1RXFUL1bits.RXFUL2) && (buffer_number==2))
		/* clear flag */
		C1RXFUL1bits.RXFUL2=0;
	/* check to see if buffer 3 is full */
	else if((C1RXFUL1bits.RXFUL3) && (buffer_number==3))
		/* clear flag */
		C1RXFUL1bits.RXFUL3=0;
	else;

}

/* code for Timer1 ISR, called every 250ms*/
void __attribute__((__interrupt__, no_auto_psv)) _T1Interrupt(void)
{
    g_app_info.s_tick++; // increment the 'slow tick'

    IFS0bits.T1IF = 0; //Clear Timer1 interrupt flag

}

/* code for Timer2 ISR, called every 10ms*/
void __attribute__((__interrupt__, no_auto_psv)) _T2Interrupt(void)
{
    g_app_info.f_tick++; // we increment the variable f_tick

    IFS0bits.T2IF = 0; //Clear Timer2 interrupt flag
    
    g_app_info.counter++;
}

void __attribute__((interrupt, no_auto_psv))_C1Interrupt(void)
{
    IFS2bits.C1IF = 0; // clear interrupt flag
    if (C1INTFbits.TBIF)
    {
        C1INTFbits.TBIF = 0;
    }

    if (C1INTFbits.RBIF)
    {
    /*check to see if buffer 1 is full */
    if(C1RXFUL1bits.RXFUL1)
    {
        /* set the buffer full flag and the buffer received flag */
        canRxMessage.buffer_status = CAN_BUF_FULL;
        canRxMessage.buffer = 1;
        g_app_info.can_rx = 1;

    }
        C1INTFbits.RBIF = 0;
    }
}

void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void)
{
    while (U1STAbits.TRMT == 0); // wait for transmitter empty
    IFS0bits.U1TXIF = 0; // Clear TX1 Interrupt flag
}

void __attribute__((interrupt, no_auto_psv)) _U2TXInterrupt(void)
{
    while (U2STAbits.TRMT == 0); // wait for transmitter empty
    IFS1bits.U2TXIF = 0; // Clear TX2 Interrupt flag
}

void __attribute__((interrupt, no_auto_psv)) _U2RXInterrupt(void)
{
    IFS1bits.U2RXIF = 0; // Clear RX2 Interrupt flag
}

/******************************************************************************
 * Function:        SENT1 Tx/Rx Interrupt
 *****************************************************************************/
void __attribute__((__interrupt__, __auto_psv__)) _SENT1Interrupt(void)
{
    /* Interrupt Service Routine code goes here */
    if (SENT1CON1bits.RCVEN == 1) // was a RX message?
    {
        // Read data from SENT registers
        g_app_info.sent1_prm.datal = SENT1DATL;
        g_app_info.sent1_prm.datah = SENT1DATH; // switch data + pot

        g_app_info.sent1_prm.is_rx = TRUE; // a message was received
        //LED1 = 1;
    };
    
    if (SENT1CON1bits.RCVEN == 0) // message was sent
    {
        SENT1STATbits.SYNCTXEN = 0;
    };
    //LED1 = 1;
    IFS11bits.SENT1IF = 0; // clear interrupt flag
}
void __attribute__((__interrupt__, __auto_psv__)) _SENT2Interrupt(void)
{
    /* Interrupt Service Routine code goes here */
    if (SENT2CON1bits.RCVEN == 1) // was a RX message?
    {
        // Read data from SENT registers
        g_app_info.sent2_prm.datal = SENT2DATL;
        g_app_info.sent2_prm.datah = SENT2DATH; // switch data + pot

        g_app_info.sent2_prm.is_rx = TRUE; // a message was received
        //LED2 = 1;
    };
    
    if (SENT2CON1bits.RCVEN == 0) // message was sent
    {
        SENT2STATbits.SYNCTXEN = 0;
    };
    //LED2 = 1;
    IFS11bits.SENT2IF = 0; // clear interrupt flag
}
/******************************************************************************
 * Function:        SENT1 error interrupt
 *****************************************************************************/
void __attribute__((__interrupt__, __auto_psv__)) _SENT1ERRInterrupt(void)
{
    // Sent Error handling code here

    IFS11bits.SENT1EIF = 0; // Clear interrupt flag.
    //LED1 = 1;
    //LED2 = 1;
    //LED3 = 1;
    while (1); // sit here if error
}
void __attribute__((__interrupt__, __auto_psv__)) _SENT2ERRInterrupt(void)
{
    // Sent Error handling code here

    IFS11bits.SENT2EIF = 0; // Clear interrupt flag.
    //LED1 = 1;
    //LED2 = 1;
    //LED3 = 1;
    while (1); // sit here if error
}
//------------------------------------------------------------------------------
//    DMA interrupt handlers
//------------------------------------------------------------------------------

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void)
{
    IFS0bits.DMA0IF = 0; // Clear the DMA0 Interrupt Flag;
}

void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void)
{
    IFS0bits.DMA1IF = 0; // Clear the DMA1 Interrupt Flag;
}

void __attribute__((interrupt, no_auto_psv)) _DMA2Interrupt(void)
{
    IFS1bits.DMA2IF = 0; // Clear the DMA2 Interrupt Flag;
}

void __attribute__((interrupt, no_auto_psv)) _DMA3Interrupt(void)
{
    IFS2bits.DMA3IF = 0; // Clear the DMA3 Interrupt Flag;
}

void __attribute__((interrupt, auto_psv)) _DefaultInterrupt(void)
{
    //LED1 = 1;
    //LED2 = 1;
    //LED3 = 1;

    while (1);
}

void __attribute__((interrupt, auto_psv)) _OscillatorFail(void)
{
    //LED1 = 1;
    //LED2 = 1;
    //LED3 = 1;

    while (1);
}

void __attribute__((interrupt, no_auto_psv)) _MathError(void)
{
    //LED1 = 1;
    //LED2 = 1;
    //LED3 = 1;

    while (1);
}

void __attribute__((interrupt, no_auto_psv)) _StackError(void)
{
    //LED1 = 1;
    //LED2 = 1;
    //LED3 = 1;

    while (1);
}

void __attribute__((interrupt, no_auto_psv)) _AddressError(void)
{
    //LED1 = 1;
    //LED2 = 1;
    //LED3 = 1;

    while (1);

}
