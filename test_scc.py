import time
from time import sleep
from datetime import datetime
import can

bustype = 'socketcan_native'
channel = 'can0'

data_38D = [0x00, 0x00, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00]
data_420 = [0x20, 0x1E, 0x20, 0xC8, 0xB8, 0x4B, 0x6A, 0x80]
data_421 = [0x00, 0x00, 0x00, 0xFF, 0xE3, 0x7F, 0x00, 0xED]

scc_421_alive_counter = 0x00
scc_38D_alive_counter = 0x00
aeb_active = 0

def scc_check_sum(data, alive_counter):
	crc = 0x00
	crc = 0x10 - (((data[0] >> 4) + (data[0] & 0b1111) + (data[1] >> 4) + (data[1] & 0b1111) + (data[2] >> 4) + (data[2] & 0b1111) + (data[3] >> 4) + (data[3] & 0b1111) + (data[4] >> 4) + (data[4] & 0b1111) + (data[5] >> 4) + (data[5] & 0b1111) + (data[6] >> 4) + (data[6] & 0b1111) + alive_counter) & 0b1111)
	#print str(hex(crc))
	return crc

def can_send(id, dat):
  bus = can.interface.Bus(channel=channel, bustype = bustype)
  msg = can.Message(arbitration_id=id, data=[dat[0], dat[1], dat[2], dat[3], dat[4], dat[5], dat[6], dat[7]], extended_id=False)
  bus.send(msg)
  print "ID: " + str(hex(id))
  print "->" + ' '.join(str(hex(x)) for x in dat)

def main():
	scc_421_alive_counter = 0x00
	scc_38D_alive_counter = 0x00
	aeb_active = 0
	count = 0
	while True:
		if (count > 50 * 60):
			aeb_active = 1
		else:
			count = count + 1

		if scc_421_alive_counter > 0x0D:
			scc_421_alive_counter = 0
		else:
			scc_421_alive_counter = scc_421_alive_counter + 1

		if (scc_38D_alive_counter > 0x0D):
			scc_38D_alive_counter = 0
		else:
			scc_38D_alive_counter = scc_38D_alive_counter + 1

		can_send(0x420, data_420)
		sleep (0.001)

		data_421[7] = (scc_check_sum(data_421, scc_421_alive_counter) << 4) | scc_421_alive_counter
		data_421[7] = data_421[7] & 0xFF
		can_send(0x421, data_421)
		sleep (0.001)

		if (aeb_active == 1):
			data_38D[0] = 0x56
			data_38D[1] = 0x14
			data_38D[2] = 0x48
			data_38D[3] = 0x80
		data_38D[7] = (scc_check_sum(data_38D, scc_38D_alive_counter) << 4) | scc_38D_alive_counter
		data_38D[7] = data_38D[7] & 0xFF
		can_send(0x38D, data_38D)
		sleep (0.018)

		##date_time = datetime.now()
		##print (date_time)

if __name__ == "__main__":
	main()
