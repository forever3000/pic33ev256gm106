#!/usr/bin/env python3

import sys
import re
import copy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from collections import defaultdict

mng = plt.get_current_fig_manager()
mng.window.set_default_size(1920, 1080)

sent1_seg1_dict = defaultdict(list)
ccan_angle_dict = defaultdict(list)

class SENT_INFO:
    def __init__(self):
        self.sts = 0
        self.angle = 0
        self.torque = 0
        self.crc = 0

    def print(self):
        print(self.sts, self.angle, self.torque, self.crc)

def getSENTInfo(can_msg):
    ret = SENT_INFO()
    #print(can_msg, can_msg[1:4])
    ret.sts = int(can_msg[0],16)
    ret.angle = int(can_msg[1:4],16)
    torque = can_msg[4:7]
    ret.torque = int(torque[::-1],16)
    ret.crc = int(can_msg[7],16)
    return ret

def main(filename):
    f = open(filename, "r")
    lines = f.readlines()
    f.close()
    times = []       #SENT timing
    angles1 = []     #SENT1 angle
    torques1 = []    #SENT1 torque
    angles2 = []     #SENT2 angle
    torques2 = []    #SENT2 torque
    ccan_times = []  #CCAN timing
    ccan_angles = [] #CCAN angle
    timebase = 0
    is_can = False

    angle1_sum = 0
    angle1_sums = []
    prev_angle1 = 0
    prev_torque1 = 0
    torque1_sum = 0
    torque1_sums = []

    for i,line in enumerate(lines):
        _, time, _, can_num, can_id, data, _ = re.split(r'\(|\)|#| |\n', line)
        time = float(time)
        #if timebase > 0 and time-timebase>=4:
        #    break
        if i==0:
            timebase = time
        if can_num=="can0" and is_can:
            times.append(time - timebase)

            #SENT1 msg
            sent_msg = getSENTInfo(data)
            angles1.append(sent_msg.angle)
            torques1.append(sent_msg.torque)

            if abs(prev_angle1-sent_msg.angle) > 0xC00: # big enough?
                diff = ( sent_msg.angle + 0xFFF - prev_angle1, sent_msg.angle-0xFFF-prev_angle1)[sent_msg.angle > 0xC00]
                angle1_sum = angle1_sum + diff
            else:
                angle1_sum = angle1_sum + (sent_msg.angle-prev_angle1)
            prev_angle1 = sent_msg.angle
            angle1_sums.append(angle1_sum)

            if abs(prev_torque1-sent_msg.torque) > 0xC00: # big enough?
                diff = ( sent_msg.torque + 0xFFF - prev_torque1, sent_msg.torque-0xFFF-prev_torque1)[sent_msg.torque > 0xC00]
                torque1_sum = torque1_sum + diff
            else:
                torque1_sum = torque1_sum + (sent_msg.torque-prev_torque1)
            prev_torque1 = sent_msg.torque
            torque1_sums.append(torque1_sum)

            #SENT2 msg
            sent_msg = getSENTInfo(data[8:])
            angles2.append(sent_msg.angle)
            torques2.append(sent_msg.torque)

            is_can = False
        elif can_num=="can1" and can_id=='2B0':
            ccan_times.append(time - timebase)
            angle = int('{}{}'.format(data[2:4],data[0:2]),16)
            angle = np.int16(angle)
            #if (angle >> 15) != 0:
            #    angle = angle - 0x10000
            ccan_angles.append(angle)
            is_can = True

    print(max(angle1_sums))
    print(max(torque1_sums))
    #mapping CCAN angle and SENT data
    # for i, val in enumerate(angle1_sums):
    #     ccan_angle_dict[np.uint16(ccan_angles[i])].append(val)
    #     ccan_angle_dict[np.uint16(ccan_angles[i])].append(torque1_sums[i])
    # for val in list(ccan_angle_dict.keys()):
    #     if len(ccan_angle_dict[val]) > 0:
    #         print(np.int16(val))
    #         print(ccan_angle_dict[val])
    #         input('Continue?')

    #reproduce SENT#1 Seg#1 value from angle1_sums
    # for i in range(0,len(angle1_sums)):
    #     if angle1_sums[i] < 0:
    #         angle1_sums[i] = 0xFFF + (angle1_sums[i]%-0xFFF)
    #     elif angle1_sums[i] > 0xFFF:
    #         angle1_sums[i] = angle1_sums[i]%0xFFF
    #     else:
    #         pass

    #     if angle1_sums[i] != angles1[i]:
    #         print(i, hex(angle1_sums[i]), hex(angles1[i]))
    #         return

    #reproduce SENT#2 Seg#2 value from torque1_sums
    # for i in range(0,len(torque1_sums)):
    #     if torque1_sums[i] < 0:
    #         torque1_sums[i] = 0xFFF + (torque1_sums[i]%-0xFFF)
    #     elif torque1_sums[i] > 0xFFF:
    #         torque1_sums[i] = torque1_sums[i]%0xFFF
    #     else:
    #         pass

    #     if torque1_sums[i] != torques1[i]:
    #         print(i, hex(torque1_sums[i]), hex(torques1[i]))
    #         return


    is_show = True
    is_plot = True #True: plot, False: scatter
    if len(times)>0 and is_show:
        plt.subplot(511)
        if is_plot:
            plt.plot(times, angles1, linewidth=1, label='SENT#1 Seg#1', color='blue')
        else:
            plt.scatter(times, angles1, s=0.1, label='SENT#1 Seg#1', color='blue')
        plt.grid()
        #plt.legend(loc=0)
        plt.title('SENT#1 Seg#1')

        plt.subplot(512)
        if is_plot:
            plt.plot(times, angle1_sums, linewidth=1, label='SENT#2 Seg#1', color='blue')
        else:
            plt.scatter(times, angles2, s=0.1, label='SENT#2 Seg#1', color='blue')
        plt.grid()
        #plt.legend(loc=2)
        plt.title('SENT#2 Seg#1')

        plt.subplot(513)
        if is_plot:
            plt.plot(times, torques1, linewidth=1, label='SENT#1 Seg#2', color='green')
        else:
            plt.scatter(times, torques1, s=0.1, label='SENT#1 Seg#2', color='green')
        plt.grid()
        #plt.legend(loc=2)
        plt.title('SENT#2 Seg#2')

        plt.subplot(514)
        if is_plot:
            plt.plot(times, torque1_sums, linewidth=1, label='SENT#2 Seg#2', color='green')
        else:
            plt.scatter(times, torques2, s=0.1, label='SENT#2 Seg#2', color='green')
        plt.grid()
        #plt.legend(loc=2)
        plt.title('SENT#2 Seg#2')

        plt.subplot(515)
        plt.plot(ccan_times, ccan_angles, linewidth=1, label='CCAN Angle', color='orange')
        #plt.scatter(ccan_times, ccan_angles, label='CCAN Angle', color='orange')
        plt.grid()
        plt.legend(loc=2)
        plt.title('CCAN angle')

        plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)
        plt.show()

main(sys.argv[1])
#main('candump-2019-08-20_110233.log')
#main('candump-2019-08-20_110336.log')
#main('candump-2019-08-20_110734.log')

#f1 = open('sent1_seg1_map.txt','w')
#f2 = open('ccan_angle_dict.txt','w')
#for i in range(0,0xFFF):
#    f1.write('{} {}\n'.format(i,len(sent1_seg1_dict[i])))
#    f1.write('{}\n'.format(sent1_seg1_dict[i]))
#for i in range(0,0xFFFF):
#    f2.write('{} {}\n'.format(i,len(sent1_seg1_dict[i])))
#    f2.write('{}\n'.format(sent1_seg1_dict[i]))
#f1.close()
#f2.close()
