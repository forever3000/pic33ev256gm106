#!/usr/bin/env python

import sys

def main():
    f = open(sys.argv[1], "r")
    for line in f.readlines():
        data = line.split('#')[1]
        sent1_h = data[1:4]
        sent1_l = data[4:7]
        sent2_h = data[7:10]
        sent2_l = data[10:13]
        print('{}, {},\t{}, {}'.format(sent1_h, sent1_l, sent2_h, sent2_l))

if __name__=="__main__":
    main()